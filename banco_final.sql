# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.35)
# Database: sgp
# Generation Time: 2017-09-24 16:58:43 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Aluno
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Aluno`;

CREATE TABLE `Aluno` (
  `idAluno` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idAluno`),
  KEY `idUsuario` (`idUsuario`),
  CONSTRAINT `aluno_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Aluno` WRITE;
/*!40000 ALTER TABLE `Aluno` DISABLE KEYS */;

INSERT INTO `Aluno` (`idAluno`, `idUsuario`)
VALUES
	(1,2);

/*!40000 ALTER TABLE `Aluno` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table AreaDePesquisa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `AreaDePesquisa`;

CREATE TABLE `AreaDePesquisa` (
  `idAreaPesquisa` int(11) NOT NULL AUTO_INCREMENT,
  `nomeArea` varchar(20) NOT NULL,
  PRIMARY KEY (`idAreaPesquisa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `AreaDePesquisa` WRITE;
/*!40000 ALTER TABLE `AreaDePesquisa` DISABLE KEYS */;

INSERT INTO `AreaDePesquisa` (`idAreaPesquisa`, `nomeArea`)
VALUES
	(1,'Teste'),
	(30,'Area Teste');

/*!40000 ALTER TABLE `AreaDePesquisa` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Arquivo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Arquivo`;

CREATE TABLE `Arquivo` (
  `idArquivo` int(11) NOT NULL AUTO_INCREMENT,
  `idAreaPesquisa` int(11) DEFAULT NULL,
  `idProfessor` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `idProjeto` int(11) DEFAULT NULL,
  `criado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idArquivo`),
  KEY `idAreaPesquisa` (`idAreaPesquisa`,`idProfessor`),
  KEY `idProfessor` (`idProfessor`),
  KEY `projetoFK` (`idProjeto`),
  CONSTRAINT `arquivo_ibfk_1` FOREIGN KEY (`idProfessor`) REFERENCES `Professor` (`idProfessor`),
  CONSTRAINT `pesquisa_FK` FOREIGN KEY (`idAreaPesquisa`) REFERENCES `AreaDePesquisa` (`idAreaPesquisa`),
  CONSTRAINT `projetoFK` FOREIGN KEY (`idProjeto`) REFERENCES `Projeto` (`idProjeto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Arquivo` WRITE;
/*!40000 ALTER TABLE `Arquivo` DISABLE KEYS */;

INSERT INTO `Arquivo` (`idArquivo`, `idAreaPesquisa`, `idProfessor`, `nome`, `endereco`, `idProjeto`, `criado`)
VALUES
	(1,NULL,1,'images.png','public/upload/images.png',1,'2017-09-18 21:17:18'),
	(3,NULL,3,'images.png','public/upload/images.png',2,'2017-09-21 16:33:56'),
	(4,1,1,'images.png','public/upload/images.png',3,'2017-09-23 14:14:36');

/*!40000 ALTER TABLE `Arquivo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Banca
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Banca`;

CREATE TABLE `Banca` (
  `idBanca` int(11) NOT NULL AUTO_INCREMENT,
  `dataRealizacao` date DEFAULT NULL,
  `observacao` varchar(45) NOT NULL,
  `localBanca` varchar(45) NOT NULL,
  `hora` time NOT NULL,
  `dataEnvio` date NOT NULL,
  `dataLimite` date NOT NULL,
  `correcoes` varchar(45) NOT NULL,
  PRIMARY KEY (`idBanca`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Banca` WRITE;
/*!40000 ALTER TABLE `Banca` DISABLE KEYS */;

INSERT INTO `Banca` (`idBanca`, `dataRealizacao`, `observacao`, `localBanca`, `hora`, `dataEnvio`, `dataLimite`, `correcoes`)
VALUES
	(1,'1111-11-11','asdsad asdasd','Faculdade','11:11:00','1111-11-11','1111-11-11',''),
	(2,'1111-03-12','sdfsdfsdfsdf','asdasd','00:00:11','1111-11-11','1111-11-11',''),
	(3,'1111-11-11','asdasd adads','Tasdasd asd','11:11:00','1111-11-11','1111-11-11','');

/*!40000 ALTER TABLE `Banca` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Cronograma
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Cronograma`;

CREATE TABLE `Cronograma` (
  `idCronograma` int(11) NOT NULL AUTO_INCREMENT,
  `idProjeto` int(11) NOT NULL,
  `dataCronograma` date NOT NULL,
  `hora` time NOT NULL,
  `assunto` varchar(20) NOT NULL,
  `sitCronograma` varchar(20) DEFAULT NULL,
  `ocorrencia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idCronograma`),
  KEY `idProjeto` (`idProjeto`),
  CONSTRAINT `cronograma_ibfk_1` FOREIGN KEY (`idProjeto`) REFERENCES `Projeto` (`idProjeto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Cronograma` WRITE;
/*!40000 ALTER TABLE `Cronograma` DISABLE KEYS */;

INSERT INTO `Cronograma` (`idCronograma`, `idProjeto`, `dataCronograma`, `hora`, `assunto`, `sitCronograma`, `ocorrencia`)
VALUES
	(1,1,'0000-00-00','11:11:00','sdsada',NULL,''),
	(2,2,'1111-11-11','11:11:00','asdads',NULL,'dasdad');

/*!40000 ALTER TABLE `Cronograma` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Professor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Professor`;

CREATE TABLE `Professor` (
  `idProfessor` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) NOT NULL,
  `titulacao` varchar(20) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  PRIMARY KEY (`idProfessor`),
  KEY `idUsuario` (`idUsuario`),
  CONSTRAINT `professor_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Professor` WRITE;
/*!40000 ALTER TABLE `Professor` DISABLE KEYS */;

INSERT INTO `Professor` (`idProfessor`, `idUsuario`, `titulacao`, `tipo`)
VALUES
	(1,3,'Teste','interno'),
	(2,4,'sdasd','interno'),
	(3,5,'dsdfsdfs','externo');

/*!40000 ALTER TABLE `Professor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table professorBanca
# ------------------------------------------------------------

DROP TABLE IF EXISTS `professorBanca`;

CREATE TABLE `professorBanca` (
  `idProfessor` int(11) NOT NULL,
  `idBanca` int(11) DEFAULT NULL,
  `idProjeto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `professorBanca` WRITE;
/*!40000 ALTER TABLE `professorBanca` DISABLE KEYS */;

INSERT INTO `professorBanca` (`idProfessor`, `idBanca`, `idProjeto`)
VALUES
	(1,2,1),
	(3,2,1),
	(2,2,1),
	(1,2,1),
	(3,3,2),
	(2,3,2),
	(1,3,2);

/*!40000 ALTER TABLE `professorBanca` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Projeto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Projeto`;

CREATE TABLE `Projeto` (
  `idProjeto` int(11) NOT NULL AUTO_INCREMENT,
  `idTema` int(11) DEFAULT NULL,
  `idAluno` int(11) DEFAULT NULL,
  `idProfessor` int(11) DEFAULT NULL,
  `idBanca` int(11) DEFAULT NULL,
  `nome` varchar(45) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `problemas` varchar(255) NOT NULL,
  `vantagens` varchar(255) NOT NULL,
  `metodologias` varchar(255) NOT NULL,
  `comentario` varchar(255) DEFAULT NULL,
  `idSituacao` int(11) DEFAULT NULL,
  `nota` float DEFAULT NULL,
  PRIMARY KEY (`idProjeto`),
  KEY `idProfessor` (`idProfessor`),
  KEY `idAluno` (`idAluno`),
  KEY `idBanca` (`idBanca`),
  KEY `idTema` (`idTema`),
  KEY `idSituacao` (`idSituacao`),
  CONSTRAINT `projeto_ibfk_1` FOREIGN KEY (`idProfessor`) REFERENCES `Professor` (`idProfessor`),
  CONSTRAINT `projeto_ibfk_2` FOREIGN KEY (`idAluno`) REFERENCES `Aluno` (`idAluno`),
  CONSTRAINT `projeto_ibfk_3` FOREIGN KEY (`idBanca`) REFERENCES `Banca` (`idBanca`),
  CONSTRAINT `projeto_ibfk_4` FOREIGN KEY (`idTema`) REFERENCES `Tema` (`idTema`),
  CONSTRAINT `projeto_ibfk_5` FOREIGN KEY (`idSituacao`) REFERENCES `Situacao` (`idSituacao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Projeto` WRITE;
/*!40000 ALTER TABLE `Projeto` DISABLE KEYS */;

INSERT INTO `Projeto` (`idProjeto`, `idTema`, `idAluno`, `idProfessor`, `idBanca`, `nome`, `descricao`, `problemas`, `vantagens`, `metodologias`, `comentario`, `idSituacao`, `nota`)
VALUES
	(1,3,1,1,2,'Teste','asdadas','asdadsas','asdas','asdadas','asdadas',7,10),
	(2,6,1,3,3,'asdasdasd asd a','asdasd','asdas','asdasd','asda','asdad as dasd',5,10),
	(3,NULL,NULL,1,NULL,'Projeto inserido pelo professor Teste','','','','',NULL,8,NULL);

/*!40000 ALTER TABLE `Projeto` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Situacao
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Situacao`;

CREATE TABLE `Situacao` (
  `idSituacao` int(11) NOT NULL AUTO_INCREMENT,
  `dsSituacao` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`idSituacao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Situacao` WRITE;
/*!40000 ALTER TABLE `Situacao` DISABLE KEYS */;

INSERT INTO `Situacao` (`idSituacao`, `dsSituacao`)
VALUES
	(1,'Analisar'),
	(2,'Aprovado'),
	(3,'Recusado'),
	(4,'Aguardando Liberação'),
	(5,'Projeto Liberado'),
	(6,'Apresentação'),
	(7,'Liberado'),
	(8,'Projeto inserido por professor');

/*!40000 ALTER TABLE `Situacao` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Tema
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Tema`;

CREATE TABLE `Tema` (
  `idTema` int(11) NOT NULL AUTO_INCREMENT,
  `nomeTema` varchar(20) NOT NULL,
  `idAreaPesquisa` int(11) NOT NULL,
  `idProfessor` int(11) NOT NULL,
  `idAluno` int(11) DEFAULT NULL,
  PRIMARY KEY (`idTema`),
  KEY `idAreaPesquisa` (`idAreaPesquisa`,`idProfessor`),
  KEY `idAluno` (`idAluno`),
  KEY `ProfessorFK` (`idProfessor`),
  CONSTRAINT `AreaPesquisaFK` FOREIGN KEY (`idAreaPesquisa`) REFERENCES `AreaDePesquisa` (`idAreaPesquisa`),
  CONSTRAINT `ProfessorFK` FOREIGN KEY (`idProfessor`) REFERENCES `Professor` (`idProfessor`),
  CONSTRAINT `tema_ibfk_2` FOREIGN KEY (`idAluno`) REFERENCES `Aluno` (`idAluno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Tema` WRITE;
/*!40000 ALTER TABLE `Tema` DISABLE KEYS */;

INSERT INTO `Tema` (`idTema`, `nomeTema`, `idAreaPesquisa`, `idProfessor`, `idAluno`)
VALUES
	(3,'Tema Teste',30,1,NULL),
	(4,'Tema Sugerido',30,1,NULL),
	(5,'Tema Sugerido',30,1,NULL),
	(6,'Tema asdada',1,1,NULL);

/*!40000 ALTER TABLE `Tema` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Usuario
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Usuario`;

CREATE TABLE `Usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `cpf` int(11) NOT NULL,
  `matricula` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `endereco` varchar(45) NOT NULL,
  `telefone` varchar(11) NOT NULL DEFAULT '',
  `dataNasc` date NOT NULL,
  `email` varchar(45) NOT NULL,
  `sexo` char(1) NOT NULL,
  `cep` char(11) NOT NULL DEFAULT '',
  `tipoUsuario` int(11) DEFAULT NULL COMMENT '1=>Admin, 2=>Aluno, 3 => Professor',
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Usuario` WRITE;
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;

INSERT INTO `Usuario` (`idUsuario`, `cpf`, `matricula`, `nome`, `endereco`, `telefone`, `dataNasc`, `email`, `sexo`, `cep`, `tipoUsuario`)
VALUES
	(1,1,1,'Joãozinho da Silva','Rua Teste','1111-1111','1111-11-11','yuri_fsm@hotmail.com','m','01311-000',1),
	(2,111,111,'Teste','Teste','11111','1111-11-11','asdsada@asdasd.com','m','11111-111',2),
	(3,222,222,'Teste','asdada','1213131','1111-11-11','asdads@sadad.com','m','121213-123',3),
	(4,555,555,'sadasda','asdad','123131231','1111-11-11','qwdasdas@asdasd.com','m','12312-123',3),
	(5,6666,6666,'dsfsdfs','asdadas','12312312','1111-11-11','asdada@asdada.com','f','123123',3);

/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
