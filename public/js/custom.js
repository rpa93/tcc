$(document).ready(function(){


  $(".submenu > a").click(function(e) {
    e.preventDefault();
    var $li = $(this).parent("li");
    var $ul = $(this).next("ul");

    if($li.hasClass("open")) {
      $ul.slideUp(350);
      $li.removeClass("open");
    } else {
      $(".nav > li > ul").slideUp(350);
      $(".nav > li").removeClass("open");
      $ul.slideDown(350);
      $li.addClass("open");
    }
  });
  
  $('#selectAreaPesquisa').change(function(){
	  $.post( BASE_URL + "Projeto/pegarTema/" + $('#selectAreaPesquisa').val(), 
		function( data ) {
		  $('#selectTema').html(data);
	  });
  });
  
$('#selectTema').change(function(){
	  
	  $.post( BASE_URL + "Projeto/pegarProfessor/" + $('#selectTema option:selected').data('professor'), 
		function( data ) {
		  $('#selectProfessor').html(data);
	  });
	  
	  if($('#selectTema option:selected').val() != 'sugerir'){
		  $('.textTema').css('display', 'none');
	  }else{
		  $('.textTema').css('display', 'block');
	  }
  });

	$('#AddProfessor').click(function(){
	  
		if($('#professoresSelecionados input[type^="hidden"]').length < 3){
		  $.post( BASE_URL + "Projeto/SelecionarProfessor/" + $('#selectProfessor option:selected').val(), 
			function( data ) {
			  $('#professoresSelecionados').append(data);
		  });
		}
	});
	
	$('.excProf').click(function(){
		
		$('#professoresSelecionados').remove('#' + $(this).data('id'));
		
		
	});
  
  	$('#RemoveProfessor').click(function(){
		
		$('#professoresSelecionados').find("tr").remove();
		
	});
  
});