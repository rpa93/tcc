<?php
class Usuario_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    
    public function getUsuario($id = null, $params = array())
    {
    	// se houver outras condições
    	if ($params['condicoes'] != null) {
    		$this->db->where($params['condicoes']);
    	}
    	
    	// Ordenar
    	if ($params['ordenar'] != null) {
    		$this->db->order_by($params['ordenar']);
    	}
    	
    	if ((isset($params['limitar'])) && (isset($params['por_pagina']))) {
    		$this->db->limit($params['por_pagina'], $params['limitar']);
    	}
    	
    	// Se forem passados mais de um id
    	if (is_array($id)) {
    		$this->db->where_in('idUsuario', $id);
    		// Se recebeu algum valor
    	} elseif ($id != '') {
    		$this->db->where('idUsuario', $id);
    	}
    	// Efetua Query
    	$query = $this->db->get('Usuario');
    	
    	return $query;
    }
    
    public function getAluno($id = null, $params = array())
    {
    	// se houver outras condições
    	if ($params['condicoes'] != null) {
    		$this->db->where($params['condicoes']);
    	}
    	
    	// Ordenar
    	if ($params['ordenar'] != null) {
    		$this->db->order_by($params['ordenar']);
    	}
    	
    	if ((isset($params['limitar'])) && (isset($params['por_pagina']))) {
    		$this->db->limit($params['por_pagina'], $params['limitar']);
    	}
    	
    	// Se forem passados mais de um id
    	if (is_array($id)) {
    		$this->db->where_in('Usuario.idUsuario', $id);
    		// Se recebeu algum valor
    	} elseif ($id != '') {
    		$this->db->where('Usuario.idUsuario', $id);
    	}
    	
    	$this->db->join('Aluno', 'Aluno.idUsuario = Usuario.idUsuario');
    	
    	// Efetua Query
    	$query = $this->db->get('Usuario');
    	
    	return $query;
    }
    
    
    public function getProfessor($id = null, $params = array())
    {
    	// se houver outras condições
    	if ($params['condicoes'] != null) {
    		$this->db->where($params['condicoes']);
    	}
    	
    	// Ordenar
    	if ($params['ordenar'] != null) {
    		$this->db->order_by($params['ordenar']);
    	}
    	
    	if ((isset($params['limitar'])) && (isset($params['por_pagina']))) {
    		$this->db->limit($params['por_pagina'], $params['limitar']);
    	}
    	
    	// Se forem passados mais de um id
    	if (is_array($id)) {
    		$this->db->where_in('Usuario.idUsuario', $id);
    		// Se recebeu algum valor
    	} elseif ($id != '') {
    		$this->db->where('Usuario.idUsuario', $id);
    	}
    	
    	$this->db->join('Professor', 'Professor.idUsuario = Usuario.idUsuario');
    	
    	// Efetua Query
    	$query = $this->db->get('Usuario');
    	
    	return $query;
    }
    

    public function create($data)
    {
    	$query = $this->db->insert('Usuario', $data);
    	
    	if ($query) {
    		$return['result'] = true;
    		//Retorna último id inserido
    		$return['id'] = $this->db->insert_id();
    	} else {
    		$return['result'] = false;
    	}
    	
    	return $return;
    }
    
    
    public function createAluno($data)
    {
    	$query = $this->db->insert('Aluno', $data);
    	
    	if ($query) {
    		$return['result'] = true;
    		//Retorna último id inserido
    		$return['id'] = $this->db->insert_id();
    	} else {
    		$return['result'] = false;
    	}
    	
    	return $return;
    }
    
    public function createProfessor($data)
    {
    	$query = $this->db->insert('Professor', $data);
    	
    	if ($query) {
    		$return['result'] = true;
    		//Retorna último id inserido
    		$return['id'] = $this->db->insert_id();
    	} else {
    		$return['result'] = false;
    	}
    	
    	return $return;
    }
    
    
    public function edit($id, $data, $where)
    {   
        if ($id != null) {
            $this->db->where('idUsuario', $id);
        }

        // se houver outras condi��es
        if ($where != null) {
            $this->db->where($where);
        }

        $query = $this->db->update('Usuario', $data);

        if ($query) {
            $return['result'] = true;
        } else {
            $return['result'] = false;
        }

        return $return;
    }

	public function editProfessor($id, $data, $where)
    {   
        if ($id != null) {
            $this->db->where('idUsuario', $id);
        }

        // se houver outras condi��es
        if ($where != null) {
            $this->db->where($where);
        }

        $query = $this->db->update('Professor', $data);

        if ($query) {
            $return['result'] = true;
        } else {
            $return['result'] = false;
        }

        return $return;
    }

    public function delete($id)
    {
        $this->db->where('idUsuario', $id);

        $query = $this->db->delete('Usuario');

        if ($query) {
            $return['result'] = true;
        } else {
            $return['result'] = false;
        }

        return $return;
    }

	public function deleteAluno($id)
    {
        $this->db->where('idUsuario', $id);

        $query = $this->db->delete('Aluno');

        if ($query) {
            $return['result'] = true;
        } else {
            $return['result'] = false;
        }

        return $return;
    }
    
    public function deleteProfessor($id)
    {
    	$this->db->where('idUsuario', $id);
    	
    	$query = $this->db->delete('Professor');
    	
    	if ($query) {
    		$return['result'] = true;
    	} else {
    		$return['result'] = false;
    	}
    	
    	return $return;
    }

}
