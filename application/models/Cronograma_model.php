<?php
class Cronograma_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    
    public function get($id = null, $params = array())
    {
    	// se houver outras condições
    	if ($params['condicoes'] != null) {
    		$this->db->where($params['condicoes']);
    	}
    	
    	// Ordenar
    	if ($params['ordenar'] != null) {
    		$this->db->order_by($params['ordenar']);
    	}
    	
    	if ((isset($params['limitar'])) && (isset($params['por_pagina']))) {
    		$this->db->limit($params['por_pagina'], $params['limitar']);
    	}
    	
    	// Se forem passados mais de um id
    	if (is_array($id)) {
    		$this->db->where_in('idCronograma', $id);
    		// Se recebeu algum valor
    	} elseif ($id != '') {
    		$this->db->where('idCronograma', $id);
    	}
    	// Efetua Query
    	$query = $this->db->get('Cronograma');
    	
    	return $query;
    }
	
    public function create($data)
    {
    	$query = $this->db->insert('Cronograma', $data);
    	
    	if ($query) {
    		$return['result'] = true;
    		//Retorna último id inserido
    		$return['id'] = $this->db->insert_id();
    	} else {
    		$return['result'] = false;
    	}
    	
    	return $return;
    }
    
    
    public function edit($id, $data, $where)
    {   
        if ($id != null) {
            $this->db->where('idCronograma', $id);
        }

        // se houver outras condi��es
        if ($where != null) {
            $this->db->where($where);
        }

        $query = $this->db->update('Cronograma', $data);

        if ($query) {
            $return['result'] = true;
        } else {
            $return['result'] = false;
        }

        return $return;
    }

    public function delete($id)
    {
        $this->db->where('idCronograma', $id);

        $query = $this->db->delete('Cronograma');

        if ($query) {
            $return['result'] = true;
        } else {
            $return['result'] = false;
        }

        return $return;
    }

}
