<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Utilidades
{

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    
    public static function date_formato($date, $hour = false)
    {
    	if ($date != '') {
    		
    		if ($hour) {
    			$hora = substr($date, 10);
    		}
    		
    		$date = substr($date, 8, 2) . '/' . substr($date, 5, 2) . '/' . substr($date, 0, 4);
    		if ($hour) {
    			$date = $date . $hora;
    		}
    		//retorna a string com a data na ordem correta e formatada
    		return $date;
    	}
    }
    
}