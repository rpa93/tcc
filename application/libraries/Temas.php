<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Temas
{

    public function __construct()
    {
        $this->CI = &get_instance();

        $this->CI->load->model('Tema_model');
    }

    public function get($id = null, $params = array())
    {
    	
    	$params['condicoes'] = array_key_exists('condicoes', $params) ? $params['condicoes'] : null;
    	$params['ordenar']      = array_key_exists('ordenar', $params) ? $params['ordenar'] : 'nomeTema ASC';
    	$params['por_pagina']      = array_key_exists('por_pagina', $params) ? $params['por_pagina'] : null;
    	$params['limitar']      = array_key_exists('limitar', $params) ? $params['limitar'] : null;
    	
    	$return = $this->CI->Tema_model->get($id, $params);
    	
    	return $return;
    }
    
    public function create($data)
    {
    	$return = $this->CI->Tema_model->create($data);
    	
    	return $return;
    }
    
    public function edit($id = null, $data, $conditions = null)
    {
    	$return = $this->CI->Tema_model->edit($id, $data, $conditions);
    	return $return['result'];
    }
    
    public function delete($id)
    {
    	$return = $this->CI->Tema_model->delete($id);
    	return $return['result'];
    }

}