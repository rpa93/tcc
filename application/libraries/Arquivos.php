<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Arquivos
{

    public function __construct()
    {
        $this->CI = &get_instance();

        $this->CI->load->model('Arquivo_model');
    }

    public function get($id = null, $params = array())
    {
    	
    	$params['condicoes'] = array_key_exists('condicoes', $params) ? $params['condicoes'] : null;
    	$params['ordenar']      = array_key_exists('ordenar', $params) ? $params['ordenar'] : 'nome ASC';
    	$params['por_pagina']      = array_key_exists('por_pagina', $params) ? $params['por_pagina'] : null;
    	$params['limitar']      = array_key_exists('limitar', $params) ? $params['limitar'] : null;
    	
    	$return = $this->CI->Arquivo_model->get($id, $params);
    	
    	return $return;
    }
    
    public function create($data)
    {
    	$upload_path_url = base_url() . 'public/uploads/';
    	
    	$config = array();
    	
    	$config['upload_path']   = 'public/uploads/';
    	$config['allowed_types'] = 'jpg|jpeg|png|pdf|PDF|gif|GIF';
    	$config['encrypt_name']  = false;
    	$config['max_size']      = '30000';
    	
    	$this->CI->load->library('upload', $config);
    		
    		$files = $_FILES;
    		for($i=0; $i< count($files['userfile']['name']); $i++)
    		{
    			$_FILES['userfile']['name']= $files['userfile']['name'][$i];
    			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
    			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
    			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
    			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
    			
    			$this->CI->upload->initialize($config);
    			$upload = $this->CI->upload->do_upload();
    			
    			
    			$info                  = array();
    			$info['nome']          = $_FILES['userfile']['name'];
    			$info['endereco']      = "public/upload/".$_FILES['userfile']['name'];
    			$info['idAreaPesquisa']= $data['idAreaPesquisa'];
    			$info['idProfessor']   = $data['idProfessor'];
    			$info['idProjeto']   = $data['idProjeto'];
    			$return[] = $this->CI->Arquivo_model->create($info);
    		}
    	
    	
    	return $return;
    }
    
    public function edit($id = null, $data, $conditions = null)
    {
    	$return = $this->CI->Arquivo_model->edit($id, $data, $conditions);
    	return $return['result'];
    }
    
    public function delete($id=null, $params = array())
    {
    	$params['condicoes'] = array_key_exists('condicoes', $params) ? $params['condicoes'] : null;
    	
    	$return = $this->CI->Arquivo_model->delete($id, $params);
    	return $return['result'];
    }

}