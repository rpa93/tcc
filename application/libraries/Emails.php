<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Emails
{

    public $send_email = 'vitor_alves_flu@hotmail.com';
    public $config     = array(
        'smtp_host'   => 'smtp.live.com',
        'smtp_user'   => 'vitor_alves_flu@hotmail.com',
        'smtp_pass'   => '#P1sc1n@#!',
        'smtp_port'   => '587',
        'protocol'    => 'smtp',
        'mailtype'    => 'html',
        'charset'     => 'utf8',
        'wordwrap'    => true,
        'smtp_crypto' => 'tls',
        'newline'     => "\r\n",
    );

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    // FUNCTIONS

    public function LoadLayout($file)
    {
        $letter = file_get_contents('public/email/padrao.htm');
        return $letter;
    }

    public function replaceContent($letter, $body = "", $title = "", $sign = "")
    {
        $str = $letter;
        if ("" . $body != "") {
            $str = str_replace("<!-- CORPO //-->", $body, $str);
        }

        if ("" . $title != "") {
            $str = str_replace("<!-- TITULO //-->", $title, $str);
        }

        if ("" . $sign != "") {
            $str = str_replace("<!-- ASSINATURA //-->", $sign, $str);
        }

        $str = str_replace("<!-- BASE_URL //-->", base_url(), $str);
        $str = str_replace("<!-- BASE_URL_IMG_EMAIL //-->", base_url().'public/general/email_layout/img/', $str);

        return $str;
    }

    public function enviaEmail($html = '', $subject = '', $to = '', $attach = array())
    {
        if (!empty($html) && is_array($to)) {
        	
            $this->CI->email->initialize($this->config);

            $this->CI->email->set_mailtype("html");

            $this->CI->email->from($this->send_email, 'SGP');
            $this->CI->email->reply_to($this->send_email, 'SGP');
            $this->CI->email->to($to['email'], $to['name']);
            //$this->CI->email->cc('another@another-example.com');
            //$this->CI->email->bcc('');
            var_dump($this->CI->email->to($to['email'], $to['name']));

            if (count($attach) > 0) {
                foreach ($attach as $key => $value) {
                    if (!empty($value)) {
                        $this->CI->email->attach($value);
                    }
                }
            } 
            
            $this->CI->email->subject($subject);
            $this->CI->email->message($html);

            if ($this->CI->email->send()) {
                //echo $this->CI->email->print_debugger();
                return true;
            } else {
                    //echo $this->CI->email->print_debugger();exit();
                return false;
            }
        }
    }

    
    public function inscricao($data)
    {
        $letter = $this->LoadLayout('padrao');
        $title  = null;
        $sign   = null;

        $body = $this->CI->load->view('email/inscricao', array('data' => $data), true);

        return $this->replaceContent($letter, $body, $title, $sign);
    }

    
    public function aprovado($data)
    {
    	$letter = $this->LoadLayout('padrao');
    	$title  = null;
    	$sign   = null;
    	
    	$body = $this->CI->load->view('email/aprovado', array('data' => $data), true);
    	
    	return $this->replaceContent($letter, $body, $title, $sign);
    }
    
    public function recusado($data)
    {
    	$letter = $this->LoadLayout('padrao');
    	$title  = null;
    	$sign   = null;
    	
    	$body = $this->CI->load->view('email/recusado', array('data' => $data), true);
    	
    	return $this->replaceContent($letter, $body, $title, $sign);
    }
    
    public function reuniao($data)
    {
        $letter = $this->LoadLayout('padrao');
        $title  = null;
        $sign   = null;

        $body = $this->CI->load->view('email/reuniao', array('data' => $data), true);

        return $this->replaceContent($letter, $body, $title, $sign);
    }
    
    public function reuniao_recusada($data)
    {
        $letter = $this->LoadLayout('padrao');
        $title  = null;
        $sign   = null;

        $body = $this->CI->load->view('email/reuniao_recusada', array('data' => $data), true);

        return $this->replaceContent($letter, $body, $title, $sign);
    }
    
    public function projetoFinal($data)
    {
        $letter = $this->LoadLayout('padrao');
        $title  = null;
        $sign   = null;

        $body = $this->CI->load->view('email/projeto-final', array('data' => $data), true);

        return $this->replaceContent($letter, $body, $title, $sign);
    }
    
    public function projetoLiberado($data)
    {
        $letter = $this->LoadLayout('padrao');
        $title  = null;
        $sign   = null;

        $body = $this->CI->load->view('email/projeto-liberado', array('data' => $data), true);

        return $this->replaceContent($letter, $body, $title, $sign);
    }
    
    public function banca($data)
    {
        $letter = $this->LoadLayout('padrao');
        $title  = null;
        $sign   = null;

        $body = $this->CI->load->view('email/banca', array('data' => $data), true);

        return $this->replaceContent($letter, $body, $title, $sign);
    }
    
    public function finalizado($data)
    {
        $letter = $this->LoadLayout('padrao');
        $title  = null;
        $sign   = null;

        $body = $this->CI->load->view('email/projeto-finalizado', array('data' => $data), true);

        return $this->replaceContent($letter, $body, $title, $sign);
    }

}
