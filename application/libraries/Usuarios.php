<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Usuarios
{

    public function __construct()
    {
        $this->CI = &get_instance();

        $this->CI->load->model('usuario_model');
    }

    public function getUsuario($id = null, $params = array())
    {
    	
    	$params['condicoes'] = array_key_exists('condicoes', $params) ? $params['condicoes'] : null;
    	$params['ordenar']      = array_key_exists('ordenar', $params) ? $params['ordenar'] : 'nome ASC';
    	$params['por_pagina']      = array_key_exists('por_pagina', $params) ? $params['por_pagina'] : null;
    	$params['limitar']      = array_key_exists('limitar', $params) ? $params['limitar'] : null;
    	
    	$return = $this->CI->usuario_model->getUsuario($id, $params);
    	
    	return $return;
    }
    
    public function getAluno($id = null, $params = array())
    {
    	
    	$params['condicoes'] = array_key_exists('condicoes', $params) ? $params['condicoes'] : null;
    	$params['ordenar']      = array_key_exists('ordenar', $params) ? $params['ordenar'] : 'nome ASC';
    	$params['por_pagina']      = array_key_exists('por_pagina', $params) ? $params['por_pagina'] : null;
    	$params['limitar']      = array_key_exists('limitar', $params) ? $params['limitar'] : null;
    	
    	$return = $this->CI->usuario_model->getAluno($id, $params);
    	
    	return $return;
    }
    
    public function getProfessor($id = null, $params = array())
    {
    	
    	$params['condicoes'] = array_key_exists('condicoes', $params) ? $params['condicoes'] : null;
    	$params['ordenar']      = array_key_exists('ordenar', $params) ? $params['ordenar'] : 'nome ASC';
    	$params['por_pagina']      = array_key_exists('por_pagina', $params) ? $params['por_pagina'] : null;
    	$params['limitar']      = array_key_exists('limitar', $params) ? $params['limitar'] : null;
    	
    	$return = $this->CI->usuario_model->getProfessor($id, $params);
    	
    	return $return;
    }
    
    
    public function create($data)
    {
    	$return = $this->CI->usuario_model->create($data);
    	
    	return $return;
    }
    
    public function createAluno($data)
    {
    	$return = $this->CI->usuario_model->createAluno($data);
    	
    	return $return;
    }
    
    
    public function createProfessor($data)
    {
    	$return = $this->CI->usuario_model->createProfessor($data);
    	
    	return $return;
    }

    
    public function edit($id = null, $data, $conditions = null)
    {
    	$return = $this->CI->usuario_model->edit($id, $data, $conditions);
    	return $return['result'];
    }
    
    
    public function editProfessor($id = null, $data, $conditions = null)
    {
    	$return = $this->CI->usuario_model->editProfessor($id, $data, $conditions);
    	return $return['result'];
    }

    
    public function delete($id)
    {
    	$return = $this->CI->usuario_model->delete($id);
    	return $return['result'];
    }
    
    public function deleteAluno($id)
    {
    	$return = $this->CI->usuario_model->deleteAluno($id);
    	return $return['result'];
    }
    
    public function deleteProfessor($id)
    {
    	$return = $this->CI->usuario_model->deleteProfessor($id);
    	return $return['result'];
    }

}
