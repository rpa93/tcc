<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tema extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('Temas');
		$this->Temas = new Temas;
		
		$this->load->library('AreasDePesquisa');
		$this->AreasDePesquisa = new AreasDePesquisa;
	}

	
	public function listar($termo='todos', $page=1)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($termo == 'todos' && $this->input->post('termo')) $termo = $this->input->post('termo');
		
		$total = $this->Temas->get(null)->num_rows();
		
		$por_pagina = 20;
		$limite = ((($page - 1) >= 0) ? $page - 1 : 0) * $por_pagina;
		
		$condicoes = array();
		$condicoes['por_pagina'] = $por_pagina;
		$condicoes['limitar'] = $limite;
		if($termo != 'todos'){
			$condicoes['condicoes'] = "nomeArea LIKE '%".$termo."%' OR Tema.idTema LIKE '%".$termo."%'";
		}
		$data['info'] = $this->Temas->get(null, $condicoes);
		
		
		$config['base_url']          = base_url() . 'tema/listar/' . $termo. '/';
		$config['total_rows']        = $total? $total: 0;
		$config['per_page']          = $por_pagina;
		$config['uri_segment']       = 4;
		$config['num_links']         = 3;
		$config['use_page_numbers']  = true;
		$config['page_query_string'] = false;
		$config['display_pages']     = true;
		$config['full_tag_open']     = '<ul class="pagination" id="pagination">';
		$config['first_link']        = false;
		$config['last_link']         = false;
		$config['next_tag_open']     = '<li class="next">';
		$config['next_link']         = '>>';
		$config['next_tag_close']    = '</li>';
		$config['prev_tag_open']     = '<li class="prev">';
		$config['prev_link']         = '<<';
		$config['prev_tag_close']    = '</li>';
		$config['cur_tag_open']      = '<li class="active"><a href="' . $config['base_url'] . $page . '">';
		$config['cur_tag_close']     = '</a></li>';
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['full_tag_close']    = '</ul>';
		
		$this->pagination->initialize($config);
		$data['paginacao'] = $this->pagination->create_links();
		
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('tema/listar', $data, true);
		$this->load->view('main', $data);
	}
	
	
	public function formulario($id=null)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		$this->form_validation->set_rules('nomeTema', 'Nome do Tema é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			
			$post= $this->input->post();
			
			$post['idAluno'] = null;
			$post['idProfessor'] = $this->session->userdata('idProfessor');
			
			if($post['idTema'] != ''){
				
				$result = $this->Temas->edit($post['idTema'], $post);
				if($result){
					$data['msg'] = "Atualizado com sucesso";
				}else{
					$data['msg'] = "Erro ao Atualizar";
				}
				
				$id = $post['idTema'];
				
			}else{
				
				//echo '<pre>'; print_r($post);exit();
				
				$result = $this->Temas->create($post);
				
				$id = $result['id'];
				
				if($result['result']){
					$data['msg'] = "Criado com sucesso";
				}else{
					$data['msg'] = "Erro ao Criar";
				}
				
			}
			
		}
		
		$data['AreaDePesquisa'] = $this->AreasDePesquisa->get(null);
		
		if($id != null){
			$data['info'] = $this->Temas->get($id)->row();
		}
		
		$this->load->view('modal/tema', $data);
	}
	
	
	public function deletar($id)
	{
		$data = array();
		
		$this->Temas->delete($id);
		
		redirect(base_url().'Tema/listar');
	}
}
