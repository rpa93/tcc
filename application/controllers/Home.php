<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('home/home', $data, true);
		$this->load->view('main', $data);
	}
	
	
}