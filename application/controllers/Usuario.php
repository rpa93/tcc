<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('Usuarios');
		$this->Usuarios = new Usuarios;
	}
	
	public function perfil()
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$id=$this->session->userdata('idUsuario');
		$data = array();
		
		$this->form_validation->set_rules('matricula', 'Matricula é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			
			$post= $this->input->post();
			
			if($post['idUsuario'] != ''){
				
				$result = $this->Usuarios->edit($post['idUsuario'], $post);
				if($result){
					$data['msg'] = "Atualizado com sucesso";
				}else{
					$data['msg'] = "Erro ao Atualizar";
				}
				
				$id = $post['idUsuario'];
				
			}
		}
		
		if($id != null){
			$data['info'] = $this->Usuarios->getProfessor($id)->row();
		}
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('usuarios/perfil', $data, true);
		$this->load->view('main', $data);
		
	}
	

	public function alunos($termo='todos', $page=1)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($termo == 'todos' && $this->input->post('termo')) $termo = $this->input->post('termo');
		
		$total = $this->Usuarios->getAluno(null)->num_rows();
		
		$por_pagina = 20;
		$limite = ((($page - 1) >= 0) ? $page - 1 : 0) * $por_pagina;
		
		$condicoes = array();
		$condicoes['por_pagina'] = $por_pagina;
		$condicoes['limitar'] = $limite;
		if($termo != 'todos'){
			$condicoes['condicoes'] = "nome LIKE '%".$termo."%' OR Usuario.idUsuario LIKE '%".$termo."%'  OR cpf LIKE '%".$termo."%'  OR endereco LIKE '%".$termo."%'  OR telefone LIKE '%".$termo."%'  OR matricula LIKE '%".$termo."%'  OR cep LIKE '%".$termo."%'  OR email LIKE '%".$termo."%'";
		}
		$data['lista_alunos'] = $this->Usuarios->getAluno(null, $condicoes);
		
		
		$config['base_url']          = base_url() . 'usuario/alunos/' . $termo. '/';
		$config['total_rows']        = $total? $total: 0;
		$config['per_page']          = $por_pagina;
		$config['uri_segment']       = 4;
		$config['num_links']         = 3;
		$config['use_page_numbers']  = true;
		$config['page_query_string'] = false;
		$config['display_pages']     = true;
		$config['full_tag_open']     = '<ul class="pagination" id="pagination">';
		$config['first_link']        = false;
		$config['last_link']         = false;
		$config['next_tag_open']     = '<li class="next">';
		$config['next_link']         = '>>';
		$config['next_tag_close']    = '</li>';
		$config['prev_tag_open']     = '<li class="prev">';
		$config['prev_link']         = '<<';
		$config['prev_tag_close']    = '</li>';
		$config['cur_tag_open']      = '<li class="active"><a href="' . $config['base_url'] . $page . '">';
		$config['cur_tag_close']     = '</a></li>';
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['full_tag_close']    = '</ul>';
		
		$this->pagination->initialize($config);
		$data['paginacao'] = $this->pagination->create_links();
		
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('usuarios/listar_alunos', $data, true);
		$this->load->view('main', $data);
	}
	
	public function orientadores($termo='todos', $page=1)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($termo == 'todos' && $this->input->post('termo')) $termo = $this->input->post('termo');
		
		$total = $this->Usuarios->getProfessor(null)->num_rows();
		
		$por_pagina = 20;
		$limite = ((($page - 1) >= 0) ? $page - 1 : 0) * $por_pagina;
		
		$condicoes = array();
		$condicoes['por_pagina'] = $por_pagina;
		$condicoes['limitar'] = $limite;
		if($termo != 'todos'){
			$condicoes['condicoes'] = "nome LIKE '%".$termo."%' OR Usuario.idUsuario LIKE '%".$termo."%'  OR cpf LIKE '%".$termo."%'  OR endereco LIKE '%".$termo."%'  OR telefone LIKE '%".$termo."%'  OR matricula LIKE '%".$termo."%'  OR cep LIKE '%".$termo."%'  OR email LIKE '%".$termo."%'";
		}
		$data['lista_professores'] = $this->Usuarios->getProfessor(null, $condicoes);
		
		
		$config['base_url']          = base_url() . 'usuario/orientadores/' . $termo. '/';
		$config['total_rows']        = $total? $total: 0;
		$config['per_page']          = $por_pagina;
		$config['uri_segment']       = 4;
		$config['num_links']         = 2;
		$config['use_page_numbers']  = true;
		$config['page_query_string'] = false;
		$config['display_pages']     = true;
		$config['full_tag_open']     = '<ul class="pagination" id="pagination">';
		$config['first_link']        = false;
		$config['last_link']         = false;
		$config['next_tag_open']     = '<li class="next">';
		$config['next_link']         = '>>';
		$config['next_tag_close']    = '</li>';
		$config['prev_tag_open']     = '<li class="prev">';
		$config['prev_link']         = '<<';
		$config['prev_tag_close']    = '</li>';
		$config['cur_tag_open']      = '<li class="active"><a href="' . $config['base_url'] . $page . '">';
		$config['cur_tag_close']     = '</a></li>';
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['full_tag_close']    = '</ul>';
		
		$this->pagination->initialize($config);
		$data['paginacao'] = $this->pagination->create_links();
		
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('usuarios/listar_orientadores', $data, true);
		$this->load->view('main', $data);
	}
	
	
	public function formularioAluno($id=null)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		$this->form_validation->set_rules('matricula', 'Matricula é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			
			$post= $this->input->post();
			
			if($post['idUsuario'] != ''){

				$result = $this->Usuarios->edit($post['idUsuario'], $post);
				if($result){
					$data['msg'] = "Atualizado com sucesso";
				}else{
					$data['msg'] = "Erro ao Atualizar";
				}
				
				$id = $post['idUsuario'];
				
			}else{
				
				$post['tipoUsuario'] = 2;
				$result = $this->Usuarios->create($post);
				
				if($result['result']){
					$this->Usuarios->createAluno(array('idUsuario' => $result['id']));
					$data['msg'] = "Criado com sucesso";
				}else{
					$data['msg'] = "Erro ao Criar";
				}
				
				$id = $result['id'];
				
			}
		}
		
		if($id != null){
			$data['aluno'] = $this->Usuarios->getAluno($id)->row();
		}
		
		$this->load->view('modal/aluno', $data);
	}
	
	
	
	public function formularioOrientador($id=null)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		$this->form_validation->set_rules('matricula', 'Matricula é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			
			$post= $this->input->post();
			
			$professor['titulacao']= $post['titulacao'];
			$professor['tipo']= $post['tipo'];
			unset($post['titulacao']);
			unset($post['tipo']);
			
			if($post['idUsuario'] != ''){
				
				$result = $this->Usuarios->edit($post['idUsuario'], $post);
				$result = $this->Usuarios->editProfessor($post['idUsuario'], $professor);
				if($result){
					$data['msg'] = "Atualizado com sucesso";
				}else{
					$data['msg'] = "Erro ao Atualizar";
				}
				
				$id = $post['idUsuario'];
				
			}else{
				$post['tipoUsuario'] = 3;
				$result = $this->Usuarios->create($post);
				$professor['idUsuario'] = $result['id'];
				
				
				if($result['result']){
					$this->Usuarios->createProfessor($professor);
					$data['msg'] = "Criado com sucesso";
				}else{
					$data['msg'] = "Erro ao Criar";
				}
				
				$id = $result['id'];
				
			}
		}
		
		if($id != null){
			$data['professor'] = $this->Usuarios->getProfessor($id)->row();
		}
		
		$this->load->view('modal/orientador', $data);

	}
	
	public function deletaProfessor($id){
		$this->Usuarios->deleteProfessor($id);
		$this->Usuarios->delete($id);
		
		redirect(base_url().'usuario/orientadores');
	}
	
	public function deletaAluno($id){
		$this->Usuarios->deleteAluno($id);
		$this->Usuarios->delete($id);
		
		redirect(base_url().'usuario/alunos');
	}
	
	
	
}