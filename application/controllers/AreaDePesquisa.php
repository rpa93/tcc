<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AreaDePesquisa extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('AreasDePesquisa');
		$this->AreasDePesquisa = new AreasDePesquisa;
		
		$this->load->library('Arquivos');
		$this->Arquivos = new Arquivos;
		
		$this->load->library('Usuarios');
		$this->Usuarios = new Usuarios;
		
		$this->load->library('Projetos');
		$this->Projetos = new Projetos;
	}

	
	public function listar($termo='todos', $page=1)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($termo == 'todos' && $this->input->post('termo')) $termo = $this->input->post('termo');
		
		$total = $this->AreasDePesquisa->get(null)->num_rows();
		
		$por_pagina = 20;
		$limite = ((($page - 1) >= 0) ? $page - 1 : 0) * $por_pagina;
		
		$condicoes = array();
		$condicoes['por_pagina'] = $por_pagina;
		$condicoes['limitar'] = $limite;
		if($termo != 'todos'){
			$condicoes['condicoes'] = "nomeArea LIKE '%".$termo."%' OR AreaDePesquisa.idAreaPesquisa LIKE '%".$termo."%'";
		}
		$data['listar'] = $this->AreasDePesquisa->get(null, $condicoes);
		
		
		$config['base_url']          = base_url() . 'AreaDePesquisa/listar/' . $termo. '/';
		$config['total_rows']        = $total? $total: 0;
		$config['per_page']          = $por_pagina;
		$config['uri_segment']       = 4;
		$config['num_links']         = 3;
		$config['use_page_numbers']  = true;
		$config['page_query_string'] = false;
		$config['display_pages']     = true;
		$config['full_tag_open']     = '<ul class="pagination" id="pagination">';
		$config['first_link']        = false;
		$config['last_link']         = false;
		$config['next_tag_open']     = '<li class="next">';
		$config['next_link']         = '>>';
		$config['next_tag_close']    = '</li>';
		$config['prev_tag_open']     = '<li class="prev">';
		$config['prev_link']         = '<<';
		$config['prev_tag_close']    = '</li>';
		$config['cur_tag_open']      = '<li class="active"><a href="' . $config['base_url'] . $page . '">';
		$config['cur_tag_close']     = '</a></li>';
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['full_tag_close']    = '</ul>';
		
		$this->pagination->initialize($config);
		$data['paginacao'] = $this->pagination->create_links();
		
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('AreaDePesquisa/listar', $data, true);
		$this->load->view('main', $data);
	}
	
	
	public function formulario($id=null)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		$this->form_validation->set_rules('nomeArea', 'Nome da Área é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			
			$post= $this->input->post();
			
			//echo '<pre>'; print_r($post);exit();
			
			if($post['idAreaPesquisa'] != ''){
				
				$result = $this->AreasDePesquisa->edit($post['idAreaPesquisa'], $post);
				if($result){
					$data['msg'] = "Atualizado com sucesso";
				}else{
					$data['msg'] = "Erro ao Atualizar";
				}
				
				$id = $post['idAreaPesquisa'];
				
			}else{
				
				$result = $this->AreasDePesquisa->create($post);
				
				$id = $result['id'];
				
				if($result['result']){
					$data['msg'] = "Criado com sucesso";
				}else{
					$data['msg'] = "Erro ao Criar";
				}
				
			}
			
			if($_FILES){
				
				
				$usuario = $this->Usuarios->getProfessor($this->session->userdata('idUsuario'))->row();
				
				$projeto = array();
				$projeto['nome'] = "Projeto inserido pelo professor ".$usuario->nome;
				$projeto['idProfessor'] = $this->session->userdata('idProfessor');
				$projeto['idAluno'] = null;
				$projeto['idSituacao'] = 8;
				$result_project = $this->Projetos->create($projeto);
				
				
				$data_pesquisa = array();
				$data_pesquisa['idAreaPesquisa'] = $id;
				$data_pesquisa['idProfessor'] = $this->session->userdata('idProfessor');
				$data_pesquisa['idProjeto'] = $result_project['id'];
 				$this->Arquivos->create($data_pesquisa);
 				
 				
 				
 				
 				
			}
			
			
		}
		
		if($id != null){
			$data['info'] = $this->AreasDePesquisa->get($id)->row();
		}
		
		$this->load->view('modal/AreaDePesquisa', $data);
	}
	
	
	public function deletar($id)
	{
		$data = array();
		
		$this->Arquivos->delete(null, array('condicoes' => 'idAreaPesquisa = '.$id));
		$this->AreasDePesquisa->delete($id);
		
		redirect(base_url().'AreaDePesquisa/listar');
	}
}
