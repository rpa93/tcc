<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('Projetos');
		$this->Projetos= new Projetos;
		
		$this->load->library('Arquivos');
		$this->Arquivos = new Arquivos;
		
		$this->load->library('Utilidades');
		$this->Utilidades = new Utilidades;
		
		$this->load->library('Usuarios');
		$this->Usuarios= new Usuarios;
		
		$this->load->library('Emails');
		$this->Emails = new Emails;
	}
	
	public function listar($termo='todos', $page=1)
	{
		$data = array();
		
		$this->form_validation->set_rules('idProjeto', 'Projeto é obrigatório', 'required');
		if ($this->form_validation->run() != false) {

			$post= $this->input->post();
			
			if($_FILES){
				$projeto = $this->Projetos->get($post['idProjeto'])->row();
				
				$data_Arquivo = array();
				$data_Arquivo['idAreaPesquisa'] = null;
				$data_Arquivo['idProfessor'] = $projeto->idProfessor;
				$data_Arquivo['idProjeto'] = $projeto->idProjeto;
				$this->Arquivos->create($data_Arquivo);
				
				$usuario = $this->Usuarios->getProfessor(null, arraY('conditions' => 'idProfessor = '.$projeto->idProfessor))->row();
				$body = $this->Emails->projetoFinal(array());
				$this->Emails->enviaEmail($body,"Projeto aguardando liberação", array('email' => $usuario->email, 'name' => $usuario->nome));
				
				
				$this->Projetos->edit($projeto->idProjeto, array('idSituacao' => 4));
			}
			
		}
		
		
		if($this->session->userdata('tipo') == 'professor'){
			$projetos = $this->Projetos->get(null,array('condicoes' => 'idProfessor = "'.$this->session->userdata('idProfessor').'" AND idSituacao < 6'))->result();
		}else{
			$projetos = $this->Projetos->get(null,array('condicoes' => 'idAluno = "'.$this->session->userdata('idAluno').'" AND idSituacao < 6'))->result();
		}
		
		foreach($projetos as $linha){
			$ids_projetos[] = $linha->idProjeto;
			$data['projetos'][$linha->idProjeto] = $linha;
		}
		
		
		if($this->session->userdata('tipo') == 'professor'){
			$projetos_enviados = $this->Projetos->get(null,array('condicoes' => 'idProfessor = "'.$this->session->userdata('idProfessor').'"'))->result();
		}else{
			$projetos_enviados = $this->Projetos->get(null,array('condicoes' => 'idAluno = "'.$this->session->userdata('idAluno').'"'))->result();
		}
		
		foreach($projetos_enviados as $linha){
			$ids_projetos_enviados[] = $linha->idProjeto;
			$data['projetos_enviados'][$linha->idProjeto] = $linha;
		}
		
		if($termo == 'todos' && $this->input->post('termo')) $termo = $this->input->post('termo');
		
		$total = $this->Arquivos->get(null)->num_rows();
		
		$por_pagina = 20;
		$limite = ((($page - 1) >= 0) ? $page - 1 : 0) * $por_pagina;
		
		$condicoes = array();
		$condicoes['por_pagina'] = $por_pagina;
		$condicoes['limitar'] = $limite;
		$condicoes['condicoes'] = "idProjeto IS NOT NULL AND idProjeto IN (".implode(',', $ids_projetos_enviados).")";
		if($termo != 'todos'){
			$condicoes['condicoes'] .= " AND nomeArea LIKE '%".$termo."%' OR Tema.idTema LIKE '%".$termo."%'";
		}
		$data['info'] = $this->Arquivos->get(null, $condicoes);
		
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('upload/listar', $data, true);
		$this->load->view('main', $data);
	}
	
	public function deletar($id)
	{
		$data = array();
		
		$this->Arquivos->delete($id);
		
		redirect(base_url().'Upload/listar');
	}
	
}
