<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projeto extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('Projetos');
		$this->Projetos= new Projetos;
		
		$this->load->library('Temas');
		$this->Temas = new Temas;
		
		$this->load->library('Usuarios');
		$this->Usuarios = new Usuarios;
		
		$this->load->library('AreasDePesquisa');
		$this->AreasDePesquisa = new AreasDePesquisa;
		
		$this->load->library('Bancas');
		$this->Bancas = new Bancas;
		
		$this->load->library('Emails');
		$this->Emails = new Emails;
		
		$this->load->library('Utilidades');
		$this->Utilidades = new Utilidades;
		
		$this->load->library('Arquivos');
		$this->Arquivos = new Arquivos;
		
		
		$situacoes = $this->db->query('SELECT * FROM Situacao')->result();
		foreach($situacoes as $linha){
			$this->situacoes[$linha->idSituacao] = $linha;
		}
	}
	
	
	public function pegarTema($idAreaPesquisa)
	{
		$data = array();
		
		$tema = $this->Temas->get(null, array('condicoes' => 'idAreaPesquisa = '.$idAreaPesquisa));
		
		echo '<option value="" data-professor="">-selecione-</option>';
		
		if($tema->num_rows()>0){
			foreach($tema->result() as $linha){
				echo '<option value="'.$linha->idTema.'" data-professor="'.$linha->idProfessor.'">'.$linha->nomeTema.'</option>';
			}
		}
		
		echo '<option value="sugerir" data-professor="">Sugerir Tema</option>';
	}
	
	public function pegarProfessor($idProfessor)
	{
		$data = array();
		
		
		if($idProfessor != 'sugerir'){
			$condicoes = array('condicoes' => array('Professor.idProfessor = '.$idProfessor));
		}else{
			$condicoes = array();
		}
		
		$usuario = $this->Usuarios->getProfessor(null, $condicoes);
		
		if($usuario->num_rows()>0){
			foreach($usuario->result() as $linha){
				echo '<option value="'.$linha->idProfessor.'">'.$linha->nome.'</option>';
			}
		}
	}
	
	
	public function analisar($termo='todos', $page=1)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($termo == 'todos' && $this->input->post('termo')) $termo = $this->input->post('termo');
		
		$total = $this->Projetos->get(null, array('condicoes' => 'idSituacao = 1 AND idProfessor = "'.$this->session->userdata('idProfessor').'"'))->num_rows();
		
		$por_pagina = 20;
		$limite = ((($page - 1) >= 0) ? $page - 1 : 0) * $por_pagina;
		
		$condicoes = array();
		$condicoes['por_pagina'] = $por_pagina;
		$condicoes['limitar'] = $limite;
		$condicoes['condicoes'] = 'idSituacao = 1 AND idProfessor = "'.$this->session->userdata('idProfessor').'"';
		if($termo != 'todos'){
			$condicoes['condicoes'] .= " AND (nome LIKE '%".$termo."%' OR descricao LIKE '%".$termo."%'  OR problemas LIKE '%".$termo."%'  OR vantagens LIKE '%".$termo."%'  OR metodologias LIKE '%".$termo."%' OR comentario LIKE '%".$termo."%')";
		}
		$data['info'] = $this->Projetos->get(null, $condicoes);
		
		foreach($this->Usuarios->getAluno(null)->result() as $linha){
			$data['alunos'][$linha->idAluno] = $linha;
		} 
		
		
		
		
		$config['base_url']          = base_url() . 'Projetos/analisar/' . $termo. '/';
		$config['total_rows']        = $total? $total: 0;
		$config['per_page']          = $por_pagina;
		$config['uri_segment']       = 4;
		$config['num_links']         = 3;
		$config['use_page_numbers']  = true;
		$config['page_query_string'] = false;
		$config['display_pages']     = true;
		$config['full_tag_open']     = '<ul class="pagination" id="pagination">';
		$config['first_link']        = false;
		$config['last_link']         = false;
		$config['next_tag_open']     = '<li class="next">';
		$config['next_link']         = '>>';
		$config['next_tag_close']    = '</li>';
		$config['prev_tag_open']     = '<li class="prev">';
		$config['prev_link']         = '<<';
		$config['prev_tag_close']    = '</li>';
		$config['cur_tag_open']      = '<li class="active"><a href="' . $config['base_url'] . $page . '">';
		$config['cur_tag_close']     = '</a></li>';
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['full_tag_close']    = '</ul>';
		
		$this->pagination->initialize($config);
		$data['paginacao'] = $this->pagination->create_links();
		
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('projetos/analisar', $data, true);
		$this->load->view('main', $data);
	}
	
	public function modalAnalisar($id=null)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($id != null){
			$data['info'] = $this->Projetos->get($id)->row();
			
			$data['tema'][$data['info']->idTema] = $this->Temas->get($data['info']->idTema)->row();
			
			$data['professores'][$data['info']->idProfessor]= $this->Usuarios->getProfessor(null, array('condicoes' => 'idProfessor = '.$data['info']->idProfessor))->row();
			
			$data['area_pesquisa'] = $this->AreasDePesquisa->get($data['tema'][$data['info']->idTema]->idAreaPesquisa)->row();
		}
		
		
		
		$this->form_validation->set_rules('comentario', 'Comentário é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			$post= $this->input->post();
			
			$result = $this->Projetos->edit($post['idProjeto'], $post);
			
			if($post['idSituacao'] == 2){
				$usuario = $this->Usuarios->getAluno(null, arraY('condicoes' => 'idAluno = '.$data['info']->idAluno))->row();
				$body = $this->Emails->aprovado(array());
				$this->Emails->enviaEmail($body,"Inscrição", array('email' => $usuario->email, 'name' => $usuario->nome));
			}else{
				$usuario = $this->Usuarios->getAluno(null, arraY('condicoes' => 'idAluno = '.$data['info']->idAluno))->row();
				$body = $this->Emails->recusado(array());
				$this->Emails->enviaEmail($body,"Inscrição", array('email' => $usuario->email, 'name' => $usuario->nome));
			}
			
			
			if($result){
				$data['msg'] = "Atualizado com sucesso";
			}else{
				$data['msg'] = "Erro ao Atualizar";
			}
			
			$id = $post['idProjeto'];
			
		}
		
		
		
		$this->load->view('modal/analisarProjeto', $data);
	}
	
	public function passo2($id=null){
		
	}
	
	
	
	public function inscricao()
	{
		$data = array();
		
		
		
		$this->form_validation->set_rules('idAreaPesquisa', 'Área de pesquisa é obrigatório', 'required');
		$this->form_validation->set_rules('idTema', 'Tema é obrigatório', 'required');
		$this->form_validation->set_rules('nome', 'Nome é obrigatório', 'required');
		$this->form_validation->set_rules('descricao', 'Descrição é obrigatório', 'required');
		$this->form_validation->set_rules('problemas', 'Descrição é obrigatório', 'required');
		$this->form_validation->set_rules('vantagens', 'Descrição é obrigatório', 'required');
		$this->form_validation->set_rules('metodologias', 'Descrição é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			
			$post= $this->input->post();
			
			
			//echo '<pre>'; print_r($post);exit();
			
			$idAreaPesquisa = $post['idAreaPesquisa'];
			unset($post['idAreaPesquisa']);
			$tema = $post['tema'];
			unset($post['tema']);
			$post['idAluno'] = $this->session->userdata('idAluno');
			$post['idBanca'] = null;
			
			
			if($post['idProjeto'] != ''){
				
				$result = $this->Projetos->edit($post['idProjeto'], $post);
				if($result){
					$data['msg'] = "Atualizado com sucesso";
				}else{
					$data['msg'] = "Erro ao Atualizar";
				}
				
				$id = $post['idProjeto'];
				
			}else{
				
				if($tema != ''){
					$create_tema['nomeTema'] = $tema;
					$create_tema['idAreaPesquisa'] = $idAreaPesquisa;
					$create_tema['idProfessor'] = $post['idProfessor'];
					$create_tema['idProfessor'] = $post['idAluno'];
					
					$result_tema = $this->Temas->create($create_tema);
					$post['idTema'] = $result_tema['id'];
				}
				
				$post['idSituacao'] = 1;
				
				$result = $this->Projetos->create($post);

				$usuario = $this->Usuarios->getProfessor(null, arraY('condicoes' => 'idProfessor = '.$post['idProfessor']))->row();
				$body = $this->Emails->inscricao(array());
				$this->Emails->enviaEmail($body,"Inscrição", array('email' => $usuario->email, 'name' => $usuario->nome));
				
				$id = $result['id'];
				
				if($result['result']){
					$data['msg'] = "Enviado com sucesso. Seu projeto será analisado.";
				}else{
					$data['msg'] = "Erro ao Criar";
				}
				
			}
			
			
		}
		
		
		
		
		$data['areaPesquisa'] = $this->AreasDePesquisa->get(null);
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('projetos/inscricao', $data, true);
		$this->load->view('main', $data);
	}
	
	
	public function aprovados($termo='todos', $page=1)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($termo == 'todos' && $this->input->post('termo')) $termo = $this->input->post('termo');
		
		$total = $this->Projetos->get(null, array('condicoes' => '((idSituacao = 2) OR (idSituacao = 8))'))->num_rows();
		
		$temas = $this->Temas->get(null)->result();
		foreach($temas as $linha){
			$data['temas'][$linha->idTema] = $linha;
		}
		
		$area_pesquisa = $this->AreasDePesquisa->get();
		foreach($area_pesquisa->result() as $row){
			$data['area_pesquisa'][$row->idAreaPesquisa] = $row;
		}
		
		$por_pagina = 20;
		$limite = ((($page - 1) >= 0) ? $page - 1 : 0) * $por_pagina;
		
		$condicoes = array();
		$condicoes['por_pagina'] = $por_pagina;
		$condicoes['limitar'] = $limite;
		$condicoes['condicoes'] = '((idSituacao = 2) OR (idSituacao = 8)) ';
		if($termo != 'todos'){
			$condicoes['condicoes'] .= " AND (nome LIKE '%".$termo."%' OR descricao LIKE '%".$termo."%'  OR problemas LIKE '%".$termo."%'  OR vantagens LIKE '%".$termo."%'  OR metodologias LIKE '%".$termo."%' OR comentario LIKE '%".$termo."%')";
		}
		
		$condicoes['condicoes'] = "idProjeto IS NOT NULL";
		$arquivos = $this->Arquivos->get(null, $condicoes);
		
		if($arquivos->num_rows()>0){
			foreach($arquivos->result() as $row){
				$data['arquivos'][$row->idProjeto] = $row;
			}
		}
		//echo '<pre>';  print_r($data['arquivos']);exit();
		
		$data['info'] = $this->Projetos->get(null, $condicoes);
		
		
		$config['base_url']          = base_url() . 'Projetos/aprovados/' . $termo. '/';
		$config['total_rows']        = $total? $total: 0;
		$config['per_page']          = $por_pagina;
		$config['uri_segment']       = 4;
		$config['num_links']         = 3;
		$config['use_page_numbers']  = true;
		$config['page_query_string'] = false;
		$config['display_pages']     = true;
		$config['full_tag_open']     = '<ul class="pagination" id="pagination">';
		$config['first_link']        = false;
		$config['last_link']         = false;
		$config['next_tag_open']     = '<li class="next">';
		$config['next_link']         = '>>';
		$config['next_tag_close']    = '</li>';
		$config['prev_tag_open']     = '<li class="prev">';
		$config['prev_link']         = '<<';
		$config['prev_tag_close']    = '</li>';
		$config['cur_tag_open']      = '<li class="active"><a href="' . $config['base_url'] . $page . '">';
		$config['cur_tag_close']     = '</a></li>';
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['full_tag_close']    = '</ul>';
		
		$this->pagination->initialize($config);
		$data['paginacao'] = $this->pagination->create_links();
		
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('projetos/aprovados', $data, true);
		$this->load->view('main', $data);
	}
	
	
	public function modalAprovados($id=null)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		$data['situacoes'] = $this->situacoes;
		
		$this->form_validation->set_rules('comentario', 'Comentário é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			$post= $this->input->post();
			
			$result = $this->Projetos->edit($post['idProjeto'], $post);
			if($result){
				$data['msg'] = "Atualizado com sucesso";
			}else{
				$data['msg'] = "Erro ao Atualizar";
			}
			
			$id = $post['idProjeto'];
			
		}
		
		if($id != null){
			$data['info'] = $this->Projetos->get($id)->row();
			
			$data['tema'][$data['info']->idTema] = $this->Temas->get($data['info']->idTema)->row();
			
			$data['area_pesquisa'] = $this->AreasDePesquisa->get($data['tema'][$data['info']->idTema]->idAreaPesquisa)->row();
			
			$data['professores'][$data['info']->idProfessor]= $this->Usuarios->getProfessor(null, array('condicoes' => 'idProfessor = '.$data['info']->idProfessor))->row();
			
			$condicoes['condicoes'] = "idProjeto = ".$id."";
			$data['arquivo'] = $this->Arquivos->get(null, $condicoes);
			
			
		}
		
		$this->load->view('modal/aprovadosProjeto', $data);
	}
	
	
	
	
	public function liberar($termo='todos', $page=1)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($termo == 'todos' && $this->input->post('termo')) $termo = $this->input->post('termo');
		
		$total = $this->Projetos->get(null,array('condicoes' => 'idSituacao = 4 AND idProfessor = "'.$this->session->userdata('idProfessor').'"'))->num_rows();
		
		$temas = $this->Temas->get(null)->result();
		foreach($temas as $linha){
			$data['temas'][$linha->idTema] = $linha;
		}

		foreach($this->Usuarios->getAluno(null)->result() as $linha){
			$data['alunos'][$linha->idAluno] = $linha;
		} 
		
		$por_pagina = 20;
		$limite = ((($page - 1) >= 0) ? $page - 1 : 0) * $por_pagina;
		
		$condicoes = array();
		$condicoes['por_pagina'] = $por_pagina;
		$condicoes['limitar'] = $limite;
		$condicoes['condicoes'] = 'idSituacao = 4 AND idProfessor = "'.$this->session->userdata('idProfessor').'"';
		if($termo != 'todos'){
			$condicoes['condicoes'] .= " AND (nome LIKE '%".$termo."%' OR descricao LIKE '%".$termo."%'  OR problemas LIKE '%".$termo."%'  OR vantagens LIKE '%".$termo."%'  OR metodologias LIKE '%".$termo."%' OR comentario LIKE '%".$termo."%')";
		}
		$data['info'] = $this->Projetos->get(null, $condicoes);
		
		
		$config['base_url']          = base_url() . 'Projetos/liberar/' . $termo. '/';
		$config['total_rows']        = $total? $total: 0;
		$config['per_page']          = $por_pagina;
		$config['uri_segment']       = 4;
		$config['num_links']         = 3;
		$config['use_page_numbers']  = true;
		$config['page_query_string'] = false;
		$config['display_pages']     = true;
		$config['full_tag_open']     = '<ul class="pagination" id="pagination">';
		$config['first_link']        = false;
		$config['last_link']         = false;
		$config['next_tag_open']     = '<li class="next">';
		$config['next_link']         = '>>';
		$config['next_tag_close']    = '</li>';
		$config['prev_tag_open']     = '<li class="prev">';
		$config['prev_link']         = '<<';
		$config['prev_tag_close']    = '</li>';
		$config['cur_tag_open']      = '<li class="active"><a href="' . $config['base_url'] . $page . '">';
		$config['cur_tag_close']     = '</a></li>';
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['full_tag_close']    = '</ul>';
		
		$this->pagination->initialize($config);
		$data['paginacao'] = $this->pagination->create_links();
		
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('projetos/liberar', $data, true);
		$this->load->view('main', $data);
	}
	
	
	
	public function liberarProjeto($id)
	{
		$data = array();
		
		$data['idProjeto'] = $id;
		
		$data['professores'] = $this->Usuarios->getProfessor(null);
		
		
		
		$this->form_validation->set_rules('idProjeto', 'Projeto é obrigatório', 'required');
		$this->form_validation->set_rules('professor[]', 'Professor é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			
			$post= $this->input->post();
			
			foreach($post['professor'] as $linha){
				$this->db->query('INSERT INTO professorBanca (idProfessor, idProjeto) VALUES ("'.$linha.'", "'.$post['idProjeto'].'")');
			}
			$this->Projetos->edit($post['idProjeto'], array('idSituacao' => 5));
			
			$id = $post['idProjeto'];
			echo 'Cadastrado com sucesso';
		}else{
			$this->load->view('modal/liberarProjeto', $data);
		}
	}
	
	public function SelecionarProfessor($id)
	{
		$data = array();
		
		$professor = $this->Usuarios->getProfessor(null, array('condicoes' => 'Professor.idProfessor = '.$id) )->row();
		
		echo '<tr id="prof_'.$professor->idProfessor.'"><td>';
		echo '&nbsp;<br>';
		echo '<b>Nome:</b> '.$professor->nome.'<br>';
		echo '<b>Email:</b> '.$professor->email.'<br>';
		echo '<b>TIpo:</b> '.$professor->tipo.'<br>';
		echo '<input type="hidden" name="professor['.$professor->idProfessor.']" value="'.$professor->idProfessor.'">';
		//echo '</td><td><button data-id="prof_'.$professor->idProfessor.'" class="excProf"><i class="fa fa-trash" aria-hidden="true"></i></button></td></tr>';
	}
	
	
	public function liberados()
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		$data['pagina_atual'] = 'projetos-liberados';
		
		$this->form_validation->set_rules('idProjeto', 'Projeto é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			redirect(base_url().'Projeto/step2/'.$this->input->post('idProjeto'));
		}else{
			$condicoes = array();
			$condicoes['condicoes'] = 'idSituacao = 5';
			$data['info'] = $this->Projetos->get(null, $condicoes);
			
			
			foreach($this->Usuarios->getAluno(null)->result() as $linha){
				$data['alunos'][$linha->idAluno] = $linha;
			}
			
			foreach($this->Usuarios->getProfessor(null)->result() as $linha){
				$data['professores'][$linha->idProfessor] = $linha;
			}
			
			
			$data['page_header'] = $this->load->view('header', $data, true);
			$data['page_footer'] = $this->load->view('footer', $data, true);
			$data['page_modals'] = $this->load->view('modals', $data, true);
			$data['page_nav'] = $this->load->view('nav', $data, true);
			$data['page_main'] = $this->load->view('projetos/liberados', $data, true);
			$this->load->view('main', $data);
		}
	}
	
	
	
	
	
	public function step2($id)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		$data['idProjeto'] = $id; 
		
		$this->form_validation->set_rules('localBanca', 'Local é obrigatório', 'required');
		$this->form_validation->set_rules('dataRealizacao', 'Data Realização é obrigatório', 'required');
		$this->form_validation->set_rules('dataEnvio', 'Data Envio é obrigatório', 'required');
		$this->form_validation->set_rules('dataLimite', 'Data Limite é obrigatório', 'required');
		$this->form_validation->set_rules('hora', 'Hora é obrigatório', 'required');
		$this->form_validation->set_rules('observacao', 'Observação é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			
			$post = $this->input->post();
			$idProjeto = $post['idProjeto'];
			unset($post['idProjeto']);
			
			$banca = $this->Bancas->create($post);
			
			
			$this->db->query('UPDATE professorBanca SET idBanca = "'.$banca['id'].'" WHERE idProjeto = "'.$idProjeto.'"');
			$this->Projetos->edit($idProjeto, array('idBanca' => $banca['id'], 'idSituacao' => 6));
			
			
			$projeto = $this->Projetos->get($idProjeto)->row();
			$usuario = $this->Usuarios->getAluno(null, arraY('condicoes' => 'idAluno = '.$projeto->idAluno))->row();
			$body = $this->Emails->banca(array());
			$this->Emails->enviaEmail($body,"Banca e apresentação definidas", array('email' => $usuario->email, 'name' => $usuario->nome));
			
			$bancaResult = $this->db->query('SELECT * FROM professorBanca WHERE idProjeto = "'.$idProjeto.'"')->result();
			
			foreach($bancaResult as $linha){
				$usuario = $this->Usuarios->getProfessor(null, arraY('condicoes' => 'idProfessor = '.$linha->idProfessor))->row();
				$body = $this->Emails->banca(array());
				$this->Emails->enviaEmail($body,"Banca e apresentação definidas", array('email' => $usuario->email, 'name' => $usuario->nome));
			}
			
			
			
			
			
			redirect(base_url().'Projeto/step3/'.$idProjeto);
		}else{
			$data['pagina_atual'] = 'projetos-liberados';
			
			$condicoes = array();
			$condicoes['condicoes'] = 'idSituacao = 5';
			$data['info'] = $this->Projetos->get(null, $condicoes);
			
			$data['page_header'] = $this->load->view('header', $data, true);
			$data['page_footer'] = $this->load->view('footer', $data, true);
			$data['page_modals'] = $this->load->view('modals', $data, true);
			$data['page_nav'] = $this->load->view('nav', $data, true);
			$data['page_main'] = $this->load->view('projetos/step2', $data, true);
			$this->load->view('main', $data);
		}
		
		
	}
	
	public function step3($id)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		$data['idProjeto'] = $id;
		
		
		$this->form_validation->set_rules('idProjeto', 'Projeto é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			
			redirect(base_url().'Projeto/step4/'.$data['idProjeto']);
		}else{
			$data['pagina_atual'] = 'projetos-liberados';
			
			$professorBanca = $this->db->query('SELECT * FROM professorBanca WHERE idProjeto = "'.$id.'"')->result();
			foreach($professorBanca as $row){
				$ids_professores[] = $row->idProfessor;
			}
			$data['professores'] = $this->Usuarios->getProfessor(null, array('condicoes' => 'idProfessor IN('.implode(',', $ids_professores).')'))->result();
			
			
			$data['page_header'] = $this->load->view('header', $data, true);
			$data['page_footer'] = $this->load->view('footer', $data, true);
			$data['page_modals'] = $this->load->view('modals', $data, true);
			$data['page_nav'] = $this->load->view('nav', $data, true);
			$data['page_main'] = $this->load->view('projetos/step3', $data, true);
			$this->load->view('main', $data);
		}
	}
	
	
	public function step4()
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		$data['pagina_atual'] = 'projetos-liberados';
		
		$condicoes = array();
		$condicoes['condicoes'] = 'idSituacao = 5';
		$data['info'] = $this->Projetos->get(null, $condicoes);
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('projetos/step4', $data, true);
		$this->load->view('main', $data);
	}
	
	
	
	public function info($termo="todos", $page=1)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($termo == 'todos' && $this->input->post('termo')) $termo = $this->input->post('termo');
		
		$total = $this->Projetos->get(null, array('condicoes' => 'idSituacao = 6 AND idProfessor = "'.$this->session->userdata('idProfessor').'"'))->num_rows();
		
		$temas = $this->Temas->get(null)->result();
		foreach($temas as $linha){
			$data['temas'][$linha->idTema] = $linha;
		}

		foreach($this->Usuarios->getAluno(null)->result() as $linha){
			$data['alunos'][$linha->idAluno] = $linha;
		} 
		
		$por_pagina = 20;
		$limite = ((($page - 1) >= 0) ? $page - 1 : 0) * $por_pagina;
		
		$condicoes = array();
		$condicoes['por_pagina'] = $por_pagina;
		$condicoes['limitar'] = $limite;
		$condicoes['condicoes'] = 'idSituacao = 6 AND idProfessor = "'.$this->session->userdata('idProfessor').'"';
		if($termo != 'todos'){
			$condicoes['condicoes'] .= " AND (nome LIKE '%".$termo."%' OR descricao LIKE '%".$termo."%'  OR problemas LIKE '%".$termo."%'  OR vantagens LIKE '%".$termo."%'  OR metodologias LIKE '%".$termo."%' OR comentario LIKE '%".$termo."%')";
		}
		$data['info'] = $this->Projetos->get(null, $condicoes);
		
		
		$config['base_url']          = base_url() . 'Projetos/aprovados/' . $termo. '/';
		$config['total_rows']        = $total? $total: 0;
		$config['per_page']          = $por_pagina;
		$config['uri_segment']       = 4;
		$config['num_links']         = 3;
		$config['use_page_numbers']  = true;
		$config['page_query_string'] = false;
		$config['display_pages']     = true;
		$config['full_tag_open']     = '<ul class="pagination" id="pagination">';
		$config['first_link']        = false;
		$config['last_link']         = false;
		$config['next_tag_open']     = '<li class="next">';
		$config['next_link']         = '>>';
		$config['next_tag_close']    = '</li>';
		$config['prev_tag_open']     = '<li class="prev">';
		$config['prev_link']         = '<<';
		$config['prev_tag_close']    = '</li>';
		$config['cur_tag_open']      = '<li class="active"><a href="' . $config['base_url'] . $page . '">';
		$config['cur_tag_close']     = '</a></li>';
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['full_tag_close']    = '</ul>';
		
		$this->pagination->initialize($config);
		$data['paginacao'] = $this->pagination->create_links();
		
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('projetos/informacoes', $data, true);
		$this->load->view('main', $data);
	}
	
	
	public function modalInfo($id)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		$condicoes = array();
		$condicoes['condicoes'] = '';
		$data['info'] = $this->Projetos->get(null, $condicoes)->row();
		
		$data['professor'][$data['info']->idProfessor]= $this->Usuarios->getProfessor(null, array('condicoes' => 'idProfessor = '.$data['info']->idProfessor))->row();
		$data['aluno'][$data['info']->idAluno]= $this->Usuarios->getAluno(null, array('condicoes' => 'idAluno = '.$data['info']->idAluno))->row();
		
		
		$professorBanca = $this->db->query('SELECT * FROM professorBanca WHERE idProjeto = "'.$id.'"')->result();
		foreach($professorBanca as $row){
			$ids_professores[] = $row->idProfessor;
		}
		$data['professores'] = $this->Usuarios->getProfessor(null, array('condicoes' => 'idProfessor IN('.implode(',', $ids_professores).')'))->result();
		
		$data['banca'] = $this->Bancas->get($data['info']->idBanca)->row();
		
		
		
		if ($this->input->post()) {
			$post = $this->input->post();
			
			if($post['nota'] != ''){
				$post['idSituacao'] = 7;
				unset($post['correcoes']);
				$idProjeto = $post['idProjeto'];
				$result = $this->Projetos->edit($idProjeto, $post);
				
				$projeto = $this->Projetos->get($idProjeto)->row();
				$usuario = $this->Usuarios->getAluno(null, arraY('condicoes' => 'idAluno = '.$projeto->idAluno))->row();
				$body = $this->Emails->finalizado(array());
				$this->Emails->enviaEmail($body,"Finalizado", array('email' => $usuario->email, 'name' => $usuario->nome));
			}
			
			if(isset($post['correcoes']) && $post['correcoes'] != ''){
				unset($post['nota']);
				$result = $this->Bancas->edit($post['idBanca'], $post);
			}
			
			
			if($result){
				$data['msg'] = "Atualizado com sucesso";
			}else{
				$data['msg'] = "Erro ao Atualizar";
			}
			
			$id = $post['idProjeto'];
			
		}else{
			$this->load->view('modal/informacoes', $data);
		}
	}
	
	public function finalizados($termo="todos", $page=1)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($termo == 'todos' && $this->input->post('termo')) $termo = $this->input->post('termo');
		
		$total = $this->Projetos->get(null, array('condicoes' => 'idSituacao = 7 AND idProfessor = '.$this->session->userdata('idProfessor')))->num_rows();
		
		$temas = $this->Temas->get(null)->result();
		foreach($temas as $linha){
			$data['temas'][$linha->idTema] = $linha;
		}
		
		$por_pagina = 20;
		$limite = ((($page - 1) >= 0) ? $page - 1 : 0) * $por_pagina;
		
		$condicoes = array();
		$condicoes['por_pagina'] = $por_pagina;
		$condicoes['limitar'] = $limite;
		$condicoes['condicoes'] = 'idSituacao = 7 ';
		
		if($this->session->userdata('tipo') == 'professor'){
			$condicoes['condicoes'] .= ' AND idProfessor = "'.$this->session->userdata('idProfessor').'" ';
		}
		if($this->session->userdata('tipo') == 'aluno'){
			$condicoes['condicoes'] .= ' AND idAluno = "'.$this->session->userdata('idAluno').'" ';
		}
		
		if($termo != 'todos'){
			$condicoes['condicoes'] .= " AND (nome LIKE '%".$termo."%' OR descricao LIKE '%".$termo."%'  OR problemas LIKE '%".$termo."%'  OR vantagens LIKE '%".$termo."%'  OR metodologias LIKE '%".$termo."%' OR comentario LIKE '%".$termo."%')";
		}
		$data['info'] = $this->Projetos->get(null, $condicoes);
		
		
		$config['base_url']          = base_url() . 'Projetos/finalizados/' . $termo. '/';
		$config['total_rows']        = $total? $total: 0;
		$config['per_page']          = $por_pagina;
		$config['uri_segment']       = 4;
		$config['num_links']         = 3;
		$config['use_page_numbers']  = true;
		$config['page_query_string'] = false;
		$config['display_pages']     = true;
		$config['full_tag_open']     = '<ul class="pagination" id="pagination">';
		$config['first_link']        = false;
		$config['last_link']         = false;
		$config['next_tag_open']     = '<li class="next">';
		$config['next_link']         = '>>';
		$config['next_tag_close']    = '</li>';
		$config['prev_tag_open']     = '<li class="prev">';
		$config['prev_link']         = '<<';
		$config['prev_tag_close']    = '</li>';
		$config['cur_tag_open']      = '<li class="active"><a href="' . $config['base_url'] . $page . '">';
		$config['cur_tag_close']     = '</a></li>';
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['full_tag_close']    = '</ul>';
		
		$this->pagination->initialize($config);
		$data['paginacao'] = $this->pagination->create_links();
		
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('projetos/finalizados', $data, true);
		$this->load->view('main', $data);
	}
	
	
}
