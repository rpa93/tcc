<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronograma extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('Cronogramas');
		$this->Cronogramas= new Cronogramas;
		
		$this->load->library('Projetos');
		$this->Projetos = new Projetos;
		
		$this->load->library('Usuarios');
		$this->Usuarios= new Usuarios;
		
		$this->load->library('Emails');
		$this->Emails = new Emails;
	}

	
	public function listar($termo='todos', $page=1)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($termo == 'todos' && $this->input->post('termo')) $termo = $this->input->post('termo');
		
		
		if($this->session->userdata('tipo') == 'professor'){
			$data['projetos'] = $this->Projetos->get(null,array('conditions' => 'idProfessor = "'.$this->session->userdata('idProfessor').'"'))->result();
		}else{
			$data['projetos'] = $this->Projetos->get(null,array('conditions' => 'idAluno = "'.$this->session->userdata('idAluno').'"'))->result();
		}
		
		foreach($data['projetos'] as $linha){
			$ids_projetos[] = $linha->idProjeto;
		}
		
		
		$total = $this->Cronogramas->get(null)->num_rows();
		
		$por_pagina = 20;
		$limite = ((($page - 1) >= 0) ? $page - 1 : 0) * $por_pagina;
		
		$condicoes = array();
		$condicoes['por_pagina'] = $por_pagina;
		$condicoes['limitar'] = $limite;
		$condicoes['condicoes'] = 'idProjeto IN ('.implode(',', $ids_projetos).')';
		if($termo != 'todos'){
			$condicoes['condicoes'] .= "dataCronograma LIKE '%".$termo."%' OR assunto LIKE '%".$termo."%' OR ocorrencia LIKE '%".$termo."%'";
		}
		$data['info'] = $this->Cronogramas->get(null, $condicoes);
		
		
		$config['base_url']          = base_url() . 'cronograma/listar/' . $termo. '/';
		$config['total_rows']        = $total? $total: 0;
		$config['per_page']          = $por_pagina;
		$config['uri_segment']       = 4;
		$config['num_links']         = 3;
		$config['use_page_numbers']  = true;
		$config['page_query_string'] = false;
		$config['display_pages']     = true;
		$config['full_tag_open']     = '<ul class="pagination" id="pagination">';
		$config['first_link']        = false;
		$config['last_link']         = false;
		$config['next_tag_open']     = '<li class="next">';
		$config['next_link']         = '>>';
		$config['next_tag_close']    = '</li>';
		$config['prev_tag_open']     = '<li class="prev">';
		$config['prev_link']         = '<<';
		$config['prev_tag_close']    = '</li>';
		$config['cur_tag_open']      = '<li class="active"><a href="' . $config['base_url'] . $page . '">';
		$config['cur_tag_close']     = '</a></li>';
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['full_tag_close']    = '</ul>';
		
		$this->pagination->initialize($config);
		$data['paginacao'] = $this->pagination->create_links();
		
		
		$data['page_header'] = $this->load->view('header', $data, true);
		$data['page_footer'] = $this->load->view('footer', $data, true);
		$data['page_modals'] = $this->load->view('modals', $data, true);
		$data['page_nav'] = $this->load->view('nav', $data, true);
		$data['page_main'] = $this->load->view('cronograma/listar', $data, true);
		$this->load->view('main', $data);
	}
	
	
	public function formulario($id=null)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$data = array();
		
		if($this->session->userdata('tipo') == 'professor'){
			$data['projetos'] = $this->Projetos->get(null,array('conditions' => 'idProfessor = "'.$this->session->userdata('idProfessor').'"'))->result();
		}else{
			$data['projetos'] = $this->Projetos->get(null,array('conditions' => 'idAluno = "'.$this->session->userdata('idAluno').'"'))->result();
		}
		
		$this->form_validation->set_rules('dataCronograma', 'Data do cronograma é obrigatório', 'required');
		if ($this->form_validation->run() != false) {
			
			$post= $this->input->post();
			
			if($post['idCronograma'] != ''){
				
				$result = $this->Cronogramas->edit($post['idCronograma'], $post);
				if($result){
					$data['msg'] = "Atualizado com sucesso";
				}else{
					$data['msg'] = "Erro ao Atualizar";
				}
				
				$id = $post['idCronograma'];
				
			}else{
				
				
				$projeto = $this->Projetos->get($post['idProjeto'])->row();
				$usuario = $this->Usuarios->getAluno(null, arraY('conditions' => 'idAluno = '.$projeto->idAluno))->row();
				$body = $this->Emails->reuniao(array());
				$this->Emails->enviaEmail($body,"Reunião", array('email' => $usuario->email, 'name' => $usuario->nome));
				
				$result = $this->Cronogramas->create($post);
				
				
				$id = $result['id'];
				
				if($result['result']){
					$data['msg'] = "Criado com sucesso";
				}else{
					$data['msg'] = "Erro ao Criar";
				}
				
			}
			
		}
		
		if($id != null){
			$data['info'] = $this->Cronogramas->get($id)->row();
		}
		
		$this->load->view('modal/cronograma', $data);
	}
	
	public function alteraStatus($id=null, $status)
	{
		if(!$this->session->userdata('logado')) redirect(base_url().'login/index/1');
		
		$result = $this->Cronogramas->edit($id, array('sitCronograma' => $status));
		
		redirect(base_url().'Cronograma/listar');
	}
	
	
	public function deletar($id)
	{
		$data = array();
		
		$this->Cronogramas->delete($id);
		
		redirect(base_url().'cronograma/listar');
	}
}
