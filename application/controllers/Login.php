<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('Usuarios');
		$this->Usuarios = new Usuarios;
	}
	
	public function index($msg=null)
	{
		$data = array();
		
		if($msg == 1){
			$data['msg'] = "Usuário ou senha não encontrados";
		}
		
		$this->form_validation->set_rules('matricula', 'Matricula é obrigatório', 'required');
		$this->form_validation->set_rules('password', 'Senha é obrigatório', 'required');
		
		if ($this->form_validation->run() == false) {
			
			$this->load->view('login/login', $data);
			
		} else {
			$post= $this->input->post();
			
			$result = $this->Usuarios->getUsuario(null, array('condicoes' => 'cpf = "'.$post['password'].'" AND matricula = "'.$post['matricula'].'"'));
			
			if($result->num_rows() > 0){
				
				$idUsuario = $result->row();
				
				$this->session->set_userdata('matricula', $idUsuario->matricula);
				$this->session->set_userdata('idUsuario', $idUsuario->idUsuario);
				$this->session->set_userdata('logado', TRUE);
				
				$aluno = $this->Usuarios->getAluno(null, array('condicoes' => 'Usuario.idUsuario = "'.$idUsuario->idUsuario.'"'));
				if($aluno->num_rows() > 0 ){
					$this->session->set_userdata('tipo', 'aluno');
					$this->session->set_userdata('idAluno', $aluno->row()->idAluno);
				}else{
					$professor = $this->Usuarios->getProfessor(null, array('condicoes' => 'Usuario.idUsuario = "'.$idUsuario->idUsuario.'"'));
					if($professor->num_rows() > 0 ){
						$this->session->set_userdata('tipo', 'professor');
						$this->session->set_userdata('idProfessor', $professor->row()->idProfessor);
					}else{
						$this->session->set_userdata('tipo', 'admin');
					}
					
				}
				
				
				redirect(base_url().'home');
			}else{
				
				$this->session->sess_destroy();
				
				redirect(base_url().'login/index/1');
			}
			
			
		}
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url().'login');
	}
}
