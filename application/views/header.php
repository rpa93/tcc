<!-- MENU TOP -->
    <div class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <!-- Logo -->
            <div class="logo">
              <h1><a href="<?php echo base_url(); ?>">SGP<?php echo ucwords($this->session->userdata('tipo')); ?></a></h1>
            </div>
          </div>
          <div class="col-md-2 col-md-offset-5">
            <div class="navbar navbar-inverse" role="banner">
              <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                <ul class="nav navbar-nav">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Conta <b class="caret"></b></a>
                    <ul class="dropdown-menu animated fadeInUp">
                      <li><a href="<?php echo base_url().'Usuario/perfil/' ?>">Perfil</a></li>
                      <li><a href="<?php echo base_url().'login/logout'?>">Sair</a></li>
                    </ul>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div><!-- end row --> 
      </div><!-- end container --> 
    </div><!-- end header -->