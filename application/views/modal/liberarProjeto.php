 <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="<?php echo base_url().'public/';?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url().'public/';?>css/styles.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url().'public/';?>font-awesome-4.7.0/css/font-awesome.min.css">

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="<?php echo base_url().'public/';?>vendors/form-helpers/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/tags/css/bootstrap-tags.css" rel="stylesheet">

    <link href="<?php echo base_url().'public/';?>css/form.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>css/wizard.css" rel="stylesheet">
    
    <form class="form-horizontal" method="post" action="">
		<input type="hidden" name="idProjeto" value="<?php echo $idProjeto; ?>">
		<div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Liberar Projeto</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <label for="exampleSelect1">Sugerir Professores para a bancada</label><br>
              <select id="selectProfessor">
              	<?php if($professores->num_rows() > 0){ ?>
	              	<?php foreach($professores->result() as $linha){ ?>
	              		<option value="<?php echo $linha->idProfessor; ?>"><?php echo $linha->nome; ?></option>
	              	<?php } ?>
              	<?php } ?>
              </select><br>
              <a href="javascript:" id="AddProfessor">+ Adicionar</a>
              <a href="javascript:" id="RemoveProfessor">- Remover</a> 
              <table id="professoresSelecionados">
              </table>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-success">Confirmar</button>
          </div>
		</form>
	
	<script>
		var BASE_URL = "<?php echo base_url(); ?>";
	</script>	
	<script src="<?php echo base_url().'public/';?>vendors/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url().'public/';?>js/custom.js"></script>