 <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="<?php echo base_url().'public/';?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url().'public/';?>css/styles.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url().'public/';?>font-awesome-4.7.0/css/font-awesome.min.css">

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="<?php echo base_url().'public/';?>vendors/form-helpers/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/tags/css/bootstrap-tags.css" rel="stylesheet">

    <link href="<?php echo base_url().'public/';?>css/form.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>css/wizard.css" rel="stylesheet">
    
    <form class="form-horizontal" method="post" enctype="multipart/form-data" action="">
    <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Cadastro</h4>
          </div>
          <div class="modal-body">
            <fieldset>
            
            	<input type="hidden" name="idTema" value="<?php if(isset($info)) echo $info->idTema; ?>">
            
              <legend>Tema</legend> 
                
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label" for="exampleInputName1">Nome:</label>
                      <input class="form-control" placeholder="Nome" type="text" name="nomeTema" value="<?php if(isset($info)) echo $info->nomeTema; ?>">
                    </div>    
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="control-label" for="exampleInputName1">Área de Pesquisa  :</label>
                      <select class="form-control" style="display: block;" name="idAreaPesquisa">
                        <option>--selecionar--</option>
                        <?php foreach($AreaDePesquisa->result() as $row){?>
                      	  <option value="<?php echo $row->idAreaPesquisa; ?>" <?php if(isset($info) && $info->idAreaPesquisa == $row->idAreaPesquisa) echo ' selected="selected"'; ?>><?php echo $row->nomeArea; ?></option>
                        <?php } ?>
                      </select>
                    </div>    
                  </div>  
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Salvar</button>
          </div>
		</form>