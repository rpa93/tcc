 <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="<?php echo base_url().'public/';?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url().'public/';?>css/styles.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url().'public/';?>font-awesome-4.7.0/css/font-awesome.min.css">

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="<?php echo base_url().'public/';?>vendors/form-helpers/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/tags/css/bootstrap-tags.css" rel="stylesheet">

    <link href="<?php echo base_url().'public/';?>css/form.css" rel="stylesheet">
    
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="">
<input type="hidden" name="idProjeto" value="<?php echo $info->idProjeto; ?>">
<div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Proposta</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          	<?php if($arquivo->num_rows()>0){ $arquivo = $arquivo->row(); ?>
          		<a href="<?php echo base_url().'public/uploads/'.$arquivo->nome; ?>" target="_blank"><i id="icon" class="glyphicon glyphicon-folder-close"></i>&nbsp;&nbsp; Arquivo</a><br><br>
          	<?php } ?>
            
              <fieldset>
                <div class="form-group">
                  <label for="disabledTextInput">Tema:</label><br>
                  <input class="form-control" type="text" style="display:block;" disabled value="<?php echo $tema[$info->idTema]->nomeTema; ?>">
                </div>
                 <div class="form-group">
                  <label for="disabledTextInput">Professor:</label><br>
                  <input class="form-control" type="text" style="display:block;" disabled value="<?php echo $professores[$info->idProfessor]->nome; ?>">
                </div>
                <div class="form-group">
                  <label for="disabledSelect">Área de Pesquisa:</label><br>
                  <input class="form-control" type="text" style="display:block;" disabled value="<?php echo $area_pesquisa->nomeArea; ?>">
                </div>
                <div class="form-group">
                  <label for="disabledTextInput">Descrição:</label><br>
                  <textarea class="form-control" style="display: block;" rows="5" cols="130" disabled><?php echo $info->descricao; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="disabledTextInput">Problemas a serem resolvidos:</label><br>
                  <textarea class="form-control" style="display: block;" rows="5" cols="130" disabled><?php echo $info->problemas; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="disabledTextInput">Vantagem para Universidade:</label><br>
                  <textarea class="form-control" style="display: block;" rows="5" cols="130" disabled><?php echo $info->vantagens; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="disabledTextInput">Metodologias a serem utilizadas:</label><br>
                  <textarea class="form-control" style="display: block;" rows="5" cols="130" disabled><?php echo $info->metodologias; ?></textarea>
                </div>
                <div class="form-group">
                  <label for="TextInput">Status:</label><br>
                  <input class="form-control" type="text" style="display:block;" disabled value="<?php echo $situacoes[$info->idSituacao]->dsSituacao; ?>">
                </div>
              <div class="form-group">
                  <label class="control-label">Comentário:</label><br>
                  <textarea class="form-control" style="display: block;" rows="5" cols="130" disabled><?php echo $info->comentario; ?></textarea>
                </div>  
              </fieldset>
          </div>
          </form>