 <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="<?php echo base_url().'public/';?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url().'public/';?>css/styles.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url().'public/';?>font-awesome-4.7.0/css/font-awesome.min.css">

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="<?php echo base_url().'public/';?>vendors/form-helpers/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/tags/css/bootstrap-tags.css" rel="stylesheet">

    <link href="<?php echo base_url().'public/';?>css/form.css" rel="stylesheet">

		<div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Projeto</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            
                <div class="form-group">
                  <label class="col-sm-2 control-label">Nome do Projeto:</label>
                  <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $info->nome; ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Aluno:</label>
                  <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $aluno[$info->idAluno]->nome; ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Orientador:</label>
                  <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $professor[$info->idProfessor]->nome; ?></p>
                  </div>
                </div>
                <?php $i=1; foreach($professores as $linha){ ?>
	                <div class="form-group">
	                  <label class="col-sm-2 control-label">Participante <?php echo $i++?>:</label>
	                  <div class="col-sm-10">
	                    <p class="form-control-static"><?php echo $linha->nome; ?></p>
	                  </div>
	                </div>
                <?php } ?>
                
                <div class="form-group">
                  <label class="col-sm-2 control-label">Local:</label>
                  <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $banca->localBanca; ?></p>
                  </div>
                </div> 
                <div class="form-group">
                  <label class="col-sm-2 control-label">Data de realização do projeto:</label>
                  <div class="col-sm-10">
                    <p class="form-control-static"><?php echo Utilidades::date_formato($banca->dataRealizacao); ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Data de envio do projeto:</label>
                  <div class="col-sm-10">
                    <p class="form-control-static"><?php echo Utilidades::date_formato($banca->dataEnvio); ?></p>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Data limite de entrega do projeto encardenado:</label>
                  <div class="col-sm-10">
                    <p class="form-control-static"><?php echo Utilidades::date_formato($banca->dataLimite); ?></p>
                  </div>
                </div>   
                <div class="form-group">
                  <label class="col-sm-2 control-label">Hora:</label>
                  <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $banca->hora; ?></p>
                  </div>
                </div> 
                <div class="form-group">
                  <label class="col-sm-2 control-label">Observação:</label>
                  <div class="col-sm-10">
                    <p class="form-control-static"><?php echo $banca->observacao; ?></p>
                  </div>
                </div> 
<?php if($info->idSituacao != 7){?>
<form class="form-horizontal" method="post">

				<input type="hidden" name="idProjeto" value="<?php echo $info->idProjeto; ?>">
				<input type="hidden" name="idBanca" value="<?php echo $info->idBanca; ?>">

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="exampleInputName1">Nota:</label>
                    <div class="col-sm-10">
                      <input class="form-control" placeholder="Nota" type="text" name="nota" value="<?php echo $info->nota; ?>">
                    </div>  
                  </div>    
                 
                
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="exampleInputName1">Correções/ Sugestões:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="" cols="136" rows="5" name="correcoes"><?php echo $banca->correcoes; ?></textarea>
                    </div>
                  </div> 

          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Salvar</a>
            </button>
          </div>
	</form>
<?php }else{ ?>

		 		<div class="form-group">
                    <label class="col-sm-2 control-label" for="exampleInputName1">Nota:</label>
                    <div class="col-sm-10">
                      <p class="form-control-static"><?php echo $info->nota; ?></p>
                    </div>  
                  </div>    
                 
                
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="exampleInputName1">Correções/ Sugestões:</label>
                    <div class="col-sm-10">
                      <p class="form-control-static"><?php echo $info->comentario; ?></p>
                    </div>
                  </div> 



<?php } ?>
<div class="modal-footer">
            <input type="button" class="btn btn-default" name="imprimir" value="Imprimir" onclick="window.print();"></a></button>
</div>