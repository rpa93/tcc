 <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="<?php echo base_url().'public/';?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url().'public/';?>css/styles.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url().'public/';?>font-awesome-4.7.0/css/font-awesome.min.css">

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="<?php echo base_url().'public/';?>vendors/form-helpers/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/tags/css/bootstrap-tags.css" rel="stylesheet">

    <link href="<?php echo base_url().'public/';?>css/form.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>css/wizard.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url().'public/';?>js/jquery-1.2.6.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'public/';?>js/jquery.maskedinput-1.1.4.pack.js"/></script>

    <script type="text/javascript">
      $(document).ready(function(){  
        $("#hora").mask("99:99");
      });
    </script>

<form class="form-horizontal" action="" method="post">
	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Reunião</h4>
          </div>
          <div class="modal-body">
                  <fieldset>
                  	<input type="hidden" name="idCronograma" value="<?php if(isset($info)) echo $info->idCronograma; ?>">
                  
                    <legend>Dados pessoais</legend>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputMatricula">Projeto:</label>
                      <div class="col-md-10">
                        <select name="idProjeto" <?php if(isset($info)) echo 'disabled'; ?>>
                        	<?php foreach($projetos as $linha){?>
                        		<option value="<?php echo $linha->idProjeto; ?>" <?php if(isset($info) && $info->idProjeto == $linha->idProjeto) echo ' selected="selected"'; ?>><?php echo $linha->nome; ?></option>
                        	<?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputMatricula">Data:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="Data" type="date" name="dataCronograma" value="<?php if(isset($info)) echo $info->dataCronograma; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputCPF">hora:</label>
                      <div class="col-md-10">
                        <input class="form-control" id="hora" placeholder="Hora" type="text" name="hora" value="<?php if(isset($info)) echo $info->hora; ?>"/>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputName1">Assunto:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="Assunto" type="text" name="assunto" value="<?php if(isset($info)) echo $info->assunto; ?>" <?php if(isset($info)) echo 'disabled'; ?>>
                      </div>
                    </div>   

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputName1">Ocorrência:</label>
                      <div class="col-md-10">
                        <textarea class="form-control" id="" cols="136" rows="5" name="ocorrencia" ><?php if(isset($info)) echo $info->ocorrencia; ?></textarea>
                      </div>
                    </div>     
                  </fieldset>
                    
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Salvar</button>
          </div>
          </form>  