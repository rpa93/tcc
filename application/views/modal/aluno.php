 <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="<?php echo base_url().'public/';?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url().'public/';?>css/styles.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url().'public/';?>font-awesome-4.7.0/css/font-awesome.min.css">

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="<?php echo base_url().'public/';?>vendors/form-helpers/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/tags/css/bootstrap-tags.css" rel="stylesheet">

    <link href="<?php echo base_url().'public/';?>css/form.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>css/wizard.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url().'public/';?>js/jquery-1.2.6.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'public/';?>js/jquery.maskedinput-1.1.4.pack.js"/></script>

    <script type="text/javascript">
      $(document).ready(function(){  
        $("#cpf").mask("999.999.999-99");
        $("#cep").mask("99999-999");
        $("#telefone").mask("(99)99999-9999");
        //$("#data").mask("99/99/9999");
      });
    </script>

    <script>

      function Onlychars(e)
      {
        var tecla=new Number();
        if(window.event) {
          tecla = e.keyCode;
        }
        else if(e.which) {
          tecla = e.which;
        }
        else {
          return true;
        }
        if((tecla >= "48") && (tecla <= "57")){
          return false;
        }
      }
    </script>

	<form class="form-horizontal" method="post" action="<?php echo base_url().'Usuario/formularioAluno';?>">
		<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Cadastro</h4>
          </div>
          <div class="modal-body">
            
                  <fieldset>
                  	<input type="hidden" name="idUsuario" value="<?php if(isset($aluno)) echo $aluno->idUsuario; ?>">
                  	
                    <legend>Dados pessoais</legend>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputMatricula">Matrícula:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="Matricula" type="text" name="matricula" value="<?php if(isset($aluno)) echo $aluno->matricula; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputCPF">CPF:</label>
                      <div class="col-md-10">
                        <input class="form-control" id="cpf" placeholder="CPF" type="text" name="cpf" value="<?php if(isset($aluno)) echo $aluno->cpf; ?>" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputName1">Nome:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="Nome" type="text" name="nome" value="<?php if(isset($aluno)) echo $aluno->nome; ?>" onkeypress="return Onlychars(event)">
                      </div>
                    </div>   
                    
                    <div class="form-group">
                      <label class="col-md-2 control-label">Sexo:</label>
                      <div class="col-md-10">
                        <div class="radio">
                          <label class="radio-inline">
                            <input type="radio" value='f' name="sexo" <?php if(isset($aluno) && $aluno->sexo == 'f') echo ' checked="checked"'; ?>>Feminino
                          </label>
                          <label class="radio-inline">
                            <input type="radio" value='m' name="sexo" <?php if(isset($aluno) && $aluno->sexo == 'm') echo ' checked="checked"'; ?>>Masculino
                          </label>  
                        </div>  
                      </div>
                    </div>


                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputName2">Data de Nascimento:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="data" type="date" name="dataNasc" value="<?php if(isset($aluno)) echo $aluno->dataNasc; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputEmail1">E-mail:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="E-mail" type="email" name="email" value="<?php if(isset($aluno)) echo $aluno->email; ?>">
                      </div>
                    </div>
    
                  
                  </fieldset>
                  
                  <fieldset>
                    <legend>Endereço</legend>
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputCEP">CEP:</label>
                      <div class="col-md-10">
                        <input class="form-control" id="cep" placeholder="CEP" type="text" name="cep" value="<?php if(isset($aluno)) echo $aluno->cep; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputName2">Endereço:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="Endereço" type="text" name="endereco" value="<?php if(isset($aluno)) echo $aluno->endereco; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputTel">Telefone:</label>
                      <div class="col-md-10">
                        <input class="form-control" id="telefone" placeholder="Telefone" type="text" name="telefone" value="<?php if(isset($aluno)) echo $aluno->telefone; ?>" />
                      </div>
                    </div>
                    
                  </fieldset>
                 
                
                     
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Salvar</button>
          </div>
          </form>