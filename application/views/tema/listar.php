		<div class="col-md-9">
          
          <div class="content-box-header">
            <div class="panel-title">Tema</div>
          </div>
          <div class="content-box-large box-with-header">       
              
              <div class="row">
                <div class="col-md-4">
                  <button type="button" id="cadastrar" class="btn btn-primary btn-modal" data-toggle="modal" data-target="#Modal" data-src="<?php echo base_url().'Tema/formulario/'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Cadastrar</button>
                </div>
                <!-- INPUT BUSCAR -->
	                <div class="col-md-3 col-md-offset-4">
	                  <div class="iconInput">
	                    <form class="form-inline" role="form" method="post">
							<i class="fa fa-search" aria-hidden="true"></i>  
							<input type="search" id="search-input" class="form-control" placeholder="Buscar..." name="termo">
						</form>
	                  </div>   
	                </div>
                <!-- END INPUT BUSCAR -->
              </div>  
              <div class="row">   
              <div class="col-md-12">  
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr role="row">
                      <td>Código</td>
                      <td>Tema</td>
                      <td>Ação</td>
                    </tr>  
                  </thead>
                  <tbody>
                    
                    <?php if($info->num_rows() >0){ ?>
						<?php foreach($info->result() as $linha){ ?>
                    
                    <tr>
                      <th scope="row"><?php echo $linha->idTema?></th>
                      <td><?php echo $linha->nomeTema?></td>
                      <td>
                        <button type="button" class="btn btn-success btn-modal" data-toggle="modal"  data-target="#Modal" data-src="<?php echo base_url().'Tema/formulario/'.$linha->idTema; ?>"><i class="fa fa-eye" aria-hidden="true"></i></button>
						<button type="button" class="btn btn-warning btn-modal" data-toggle="modal"  data-target="#Modal" data-src="<?php echo base_url().'Tema/formulario/'.$linha->idTema; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></button>
						<a href="<?php echo base_url().'Tema/deletar/'.$linha->idTema; ?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                        <?php } ?>
                    <?php } ?>
                    
                  </tbody>
                </table>
              </div><!-- end col-md-12 -->      
            </div><!-- end row --> 
              <div class="row">
                        <div class="col-md-12">
                          <div class="col-md-offset-6">   
                            <?php if(isset($paginacao)) echo $paginacao;?>
                          </div> 
                        </div> 
              </div>     
          </div><!-- end row --> 
        </div>