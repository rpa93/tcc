<div class="col-md-9">
              <div class="content-box-header">
                <div class="panel-title">Liberar Projetos</div>
              </div>
              <div class="content-box-large box-with-header">
                <section>
                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">

                                <li role="presentation" class="desabled">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Projetos">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-list-alt"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Banca">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </span>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Representantes">
                                        <span class="round-tab">
                                            <i class="fa fa-users" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="active">
                                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Cadastrado">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-ok"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <form role="form">
                            <div class="tab-content">
                                <div class="tab-pane active" role="tabpanel" id="complete">
                                    <h3>Complete</h3>
                                    <p>Banca cadastrada com sucesso!</p>
                                    <a href="<?php echo base_url().'Projeto/liberados'; ?>">Voltar</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div><!-- end wizard -->
                </section>
              </div>  
            </div><!-- end col-md-9 --> 