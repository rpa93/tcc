<script type="text/javascript" src="<?php echo base_url().'public/';?>js/jquery-1.2.6.pack.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'public/';?>js/jquery.maskedinput-1.1.4.pack.js"/></script>

    <script type="text/javascript">
      $(document).ready(function(){  
        $("#hora").mask("99:99");
      });
    </script>

<div class="col-md-9">
              <div class="content-box-header">
                <div class="panel-title">Liberar Projetos</div>
              </div>
              <div class="content-box-large box-with-header">
                <section>
                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">

                                <li role="presentation" class="disabled">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Projetos">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-list-alt"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="active">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Banca">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </span>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Representantes">
                                        <span class="round-tab">
                                            <i class="fa fa-users" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Cadastrado">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-ok"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <form role="form" method="post">
                            <div class="tab-content">
                                
                                
                                <input type="hidden" name="idProjeto" value="<?php echo $idProjeto; ?>" />
                                
                                <div class="tab-pane active" role="tabpanel" id="step2">
                                    <h3>Banca</h3>
                                    <hr>
                                      <div class="row">
                                          <div class="col-md-4">
                                            <div class="form-group">
                                              <label for="exemploInputName2">Local:</label>
                                              <input type="text" class="form-control" id="exemploInputName2" placeholder="Local" name="localBanca">
                                            </div>
                                          </div> 
                                          <div class="col-md-4">
                                            <div class="form-group">
                                              <label for="exemploInputName2">Data de realização do projeto:</label>
                                              <input type="date" class="form-control" id="exemploInputName2" placeholder="Data" name="dataRealizacao">
                                            </div>
                                          </div> 
                                           <div class="col-md-4">
                                            <div class="form-group">
                                              <label for="exemploInputName2">Data do envio do projeto:</label>
                                              <input type="date" class="form-control" id="exemploInputName2" placeholder="Data" name="dataEnvio">
                                            </div>
                                          </div> 
                                           <div class="col-md-4">
                                            <div class="form-group">
                                              <label for="exemploInputName2">Data limite de entrega do projeto encadernado:</label>
                                              <input type="date" class="form-control" id="exemploInputName2" placeholder="Data" name="dataLimite">
                                            </div>
                                          </div> 
                                          <div class="col-md-4">
                                            <div class="form-group">
                                              <label for="exemploInputName2">Hora:</label>
                                              <input type="text" class="form-control" id="hora" placeholder="Hora" name="hora">
                                            </div>
                                          </div>                                                    
                                      </div>
                                      <div class="row">
                                          <div class="col-md-12">
                                            <label for="">Observação:</label>
                                            <textarea class="form-control" id="" cols="136" rows="5" name="observacao"></textarea>
                                          </div>
                                      </div>

                                    <hr>
                                    <ul class="list-inline pull-right">
                                        <li><button type="submit" class="btn btn-primary next-step">Salvar e continuar</button></li>
                                    </ul>
                                </div>
                                
                                
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div><!-- end wizard -->
                </section>
              </div>  
            </div><!-- end col-md-9 --> 