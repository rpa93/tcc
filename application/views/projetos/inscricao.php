<div class="col-md-9">
          <div class="content-box-header">
            <div class="panel-title">Formulário de Inscrição</div>  
          </div>
          <div class="content-box-large box-with-header">
            <div class="wizard">
                <!-- icones dos tabs -->
                  <div class="wizard-inner">
                      <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active">
                              <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Projetos">
                                  <span class="round-tab">
                                      <i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp; Sugerir Tema
                                  </span>
                              </a>
                          </li>
                      </ul>
                  </div><!-- end icones dos tabs -->
                  <div class="row">
                    <div class="col-md-12">
                        <div class="tab-content">
                          <!-- tab 1 - tema -->
                          
                          <?php if(isset($msg)) echo $msg?>
                          
                          <form class="form-inline" method="post">
                          
                          <input type="hidden" name="idProjeto" value="" />
                          
                          <div class="tab-pane active" role="tabpanel" id="step1">
                            <div class="col-md-12"> 
                              <div class="step11">
                                  <br>
                                  <h3>Tema</h3>
                                  <hr>
                                  <div class="row">
                                      <div class="col-md-4">
                                        <div class="form-group" id="form_inscricao">
                                          <label class="control-label">Área de Pesquisa:</label>
                                          <select class="form-control" style="display:block;" name="idAreaPesquisa" id="selectAreaPesquisa">
                                            <option>-- selecionar --</option>
                                            <?php if($areaPesquisa->num_rows() > 0){ ?>
                                            <?php foreach($areaPesquisa->result() as $row){ ?>
                                            	<option value="<?php echo $row->idAreaPesquisa; ?>"><?php echo $row->nomeArea; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                          </select>
                                        </div>
                                      </div> 
                                      <div class="col-md-4">         
                                        <div class="form-group" id="form_inscricao">
                                          <label class="control-label">Tema:</label>
                                          <select class="form-control" style="display:block;" name="idTema" id="selectTema">
                                          	<option>-- selecionar --</option>
                                          </select>
                                        </div>
                                      </div>
                                      
                                      <div class="col-md-4">    
                                        <div class="form-group" id="form_inscricao">
                                          <label class="control-label">Professor:</label>
                                          <select class="form-control" style="display:block;" name="idProfessor" id="selectProfessor">
                                            <option>-- selecionar --</option>
                                          </select>
                                        </div>
                                      </div>
                                       
                                       <div class="col-md-12">  
	                                       <div class="col-md-4">         
	                                        <div class="form-group" id="form_inscricao">
	                                          <label class="control-label">Nome do Projeto:</label>
	                                          <input class="form-control" type="text" placeholder="Nome do Projeto" style="display:block;" name="nome">
	                                        </div>
	                                      </div>  
	                                      <div class="col-md-offset-8 textTema" style="display:none;">         
	                                        <div class="form-group" id="form_inscricao">
	                                          <label class="control-label">Tema:</label>
	                                          <input class="form-control" type="text" placeholder="Tema" name="tema">
	                                        </div>
	                                      </div>
                                      </div>
                                      <div class="col-md-12">                                       
                                            <div class="form-group" id="form_inscricao">
                                              <label class="control-label">Descrição:</label>
                                              <textarea class="form-control" placeholder="Descrição.." style="display: block;" rows="5" cols="130" name="descricao"></textarea>
                                            </div>  
                                        </div> 
                                        <div class="col-md-12">
                                        
                                            <div class="form-group" id="form_inscricao">
                                              <label class="control-label">Problemas a serem resolvidos:</label>
                                              <textarea class="form-control" placeholder="Problemas.." style="display: block;" rows="5" cols="130" name="problemas"></textarea>
                                            </div>  
                                          
                                        </div>     
                                        <div class="col-md-12">
                                            <div class="form-group" id="form_inscricao">
                                              <label class="control-label">Vantagem para universidade:</label>
                                              <textarea class="form-control" placeholder="Vantagem.." style="display: block;" rows="5" cols="130" name="vantagens"></textarea>
                                            </div>  
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group" id="form_inscricao">
                                              <label class="control-label">Metodologias e tecnologias a serem utilizadas:</label>
                                              <textarea class="form-control" placeholder="Metodologias e tecnologias.." style="display: block;" rows="5" cols="130" name="metodologias"></textarea>
                                            </div>  
                                        </div>  
                                    
                                  </div>      
                                  <hr>
                                  <div class="col-md-4 col-md-offset-8" >
                                    <ul class="list-inline pull-right">
                                      <li><button type="submit" class="btn btn-primary next-step">Enviar e-mail</button></li>
                                    </ul>
                                  </div> 
                              </div>
                            </div>    
                          </div>
                          </form>
                    </div><!-- end col-md-12 -->  
                  </div><!-- end row -->  
            </div><!-- end wizard -->
          </div><!-- end content-box-large box-with-header -->
        </div><!-- end col-md-9 -->
		  </div>