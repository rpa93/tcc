<div class="col-md-9">
          <div class="content-box-header">
            <div class="panel-title">Liberação de Projetos</div>
          </div>
          <div class="content-box-large box-with-header">
            <div class="row">  
              <div class="col-md-12">
                <h4><strong>Projetos</strong></h4>
                <hr>
                <table class="table table-striped table-bordered" id="example">
                      <thead>
                        <tr role="row"></tr>
                          <td class="sorting_asc" role="columheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Codigo: active to sort column descending">Código</td>
                          <td>Projeto</td>
                          <td>Nome</td>
                          <td>Ação</td>
                         
                        </tr>  
                      </thead>
                      <tbody>
                      
                      <?php if($info->num_rows() >0){ ?>
						<?php foreach($info->result() as $linha){ ?>
                        <tr>
                          <th scope="row"><?php echo $linha->idProjeto; ?></th>
                          <td><?php echo $linha->nome; ?></td>
                          <td><?php echo $alunos[$linha->idAluno]->nome; ?></td>
                          <td>
                            <button type="button" class="btn btn-primary btn-modal" data-toggle="modal"  data-target="#Modal" data-src="<?php echo base_url().'Projeto/liberarProjeto/'.$linha->idProjeto; ?>">Liberar</button>
                          </td>
                        </tr>
                        
                        <?php }?>
                       <?php }?>
                       
                      </tbody>
                    </table>
              </div>
            </div>  
          </div>
        </div>