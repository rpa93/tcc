<div class="col-md-9">
              <div class="content-box-header">
                <div class="panel-title">Liberar Projetos</div>
              </div>
              <div class="content-box-large box-with-header">
                <section>
                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">

                                <li role="presentation" class="active">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Projetos">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-list-alt"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Banca">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </span>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Representantes">
                                        <span class="round-tab">
                                            <i class="fa fa-users" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Cadastrado">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-ok"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <form role="form" method="post">
                            <div class="tab-content">
                                <div class="tab-pane active" role="tabpanel" id="step1">
                                    <h3>Projetos</h3>
                                    <hr>
                                    <table class="table table-striped table-bordered" id="example">
                                      <thead>
                                        <tr role="row"></tr>
                                          <th>Selecionar</th>
                                          <th>Código</th>
                                          <th>Projeto</th>
                                          <th>Aluno</th>
                                          <th>Orientador</th>
                                          <th>Ação</th>
                                        </tr>  
                                      </thead>
                                      <tbody>
                                      
                                      <?php if($info->num_rows() >0){ ?>
										<?php foreach($info->result() as $linha){ ?>
                                        <tr>
                                          <td><input type="radio" name="idProjeto" value="<?php echo $linha->idProjeto; ?>"></td>
                                          <th scope="row"><?php echo $linha->idProjeto; ?></th>
                                          <td><?php echo $linha->nome; ?></td>
                                          <td><?php echo $alunos[$linha->idAluno]->nome; ?></td>
                                          <td><?php echo $professores[$linha->idProfessor]->nome; ?></td>
                                          <td>
                                            <button type="button" class="btn btn-success btn-modal" data-toggle="modal" data-target="#Modal" data-src="<?php echo base_url().'Projeto/modalAprovados/'.$linha->idProjeto; ?>"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                          </td>
                                        </tr>
                                        <?php } ?>
                                      <?php } ?>
                                      
                                      
                                      </tbody>
                                    </table>
                                    <hr>
                                    <?php if($info->num_rows() >0){ ?>
                                    <ul class="list-inline pull-right">
                                        <li><button type="submit" class="btn btn-primary next-step">Salvar e continuar</button></li>
                                    </ul>
                                    <?php } ?>
                                </div>
                                
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div><!-- end wizard -->
                </section>
              </div>  
            </div><!-- end col-md-9 --> 