<div class="col-md-9">
              <div class="content-box-header">
                <div class="panel-title">Liberar Projetos</div>
              </div>
              <div class="content-box-large box-with-header">
                <section>
                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">

                                <li role="presentation" class="disabled">
                                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Projetos">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-list-alt"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Banca">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </span>
                                    </a>
                                </li>
                                <li role="presentation" class="active">
                                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Representantes">
                                        <span class="round-tab">
                                            <i class="fa fa-users" aria-hidden="true"></i>
                                        </span>
                                    </a>
                                </li>

                                <li role="presentation" class="disabled">
                                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Cadastrado">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-ok"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <form role="form" method="post">
                        	<input type="hidden" name="idProjeto" value="<?php echo $idProjeto; ?>" />
                            <div class="tab-content">
                                
                                <div class="tab-pane active" role="tabpanel" id="step3">
                                    <h3>Representantes da Banca</h3>
                                    <hr>
                                    <div class="row">
                                      
                                      <?php $i=1; foreach($professores as $linha){ ?>
                                      
                                      <div class="col-md-3">   
                                        <h4>Participante <?php echo $i++; ?></h4>
                                          <fieldset disabled>
                                            <div class="form-group">
                                              <label for="disabledTextInput">Nome:</label>
                                              <input type="text" id="disabledTextInput" class="form-control" placeholder="<?php echo $linha->nome; ?>">
                                            </div>
                                            <div class="form-group">
                                              <label for="disabledTextInput">CPF:</label>
                                              <input type="text" id="disabledTextInput" class="form-control" placeholder="<?php echo $linha->cpf; ?>">
                                            </div>
                                            <div class="form-group">
                                              <label for="disabledTextInput">E-mail:</label>
                                              <input type="text" id="disabledTextInput" class="form-control" placeholder="<?php echo $linha->email; ?>">
                                            </div>
                                            <div class="form-group">
                                              <label for="disabledSelect">Tipo</label>
                                              <select id="disabledSelect" class="form-control">
                                                <option>Professor <?php echo $linha->tipo; ?></option>
                                                
                                              </select>
                                            </div>
                                          </fieldset>  
                                      </div>
                                      <div class="col-md-1">
                                        <div class="linha-vertical"></div>
                                      </div>
                                      <?php } ?>
                                    
                                        
                                    </div>
                                    
                                    <hr>
                                    
                                     
                                    <ul class="list-inline pull-right"> 
                                        <li><button type="submit" class="btn btn-primary btn-info-full next-step">Enviar</button></li>
                                    </ul>
                                    </form>
                                </div>
                                <div class="tab-pane" role="tabpanel" id="complete">
                                    <h3>Complete</h3>
                                    <p>Banca cadastrada com sucesso!</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div><!-- end wizard -->
                </section>
              </div>  
            </div><!-- end col-md-9 --> 