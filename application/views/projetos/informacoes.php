	<div class="col-md-9">
          <div class="content-box-header">
            <div class="panel-title">Informações</div>
          </div>
          <div class="content-box-large box-with-header">
            <table class="table table-striped table-bordered" id="example">
              <thead>
                <tr role="row"></tr>
                  <th>Código</th>
                  <th>Aluno</th>
                  <th>Nome do Projeto</th>
                  <th>Ação</th>
                </tr>  
              </thead>
              <tbody>
              
              <?php if($info->num_rows() >0){ ?>
				<?php foreach($info->result() as $linha){ ?>
                <tr> 
                  <th scope="row"><?php echo $linha->idProjeto; ?></th>
                  <td><?php echo $alunos[$linha->idAluno]->nome; ?></td>
                  <td><?php echo $linha->nome; ?></td>
                   <td>
                   	<button type="button" class="btn btn-success btn-modal" data-toggle="modal"  data-target="#Modal" data-src="<?php echo base_url().'Projeto/modalInfo/'.$linha->idProjeto; ?>"><i class="fa fa-eye" aria-hidden="true"></i></button>
                  </td>
                </tr>
				<?php } ?>
			<?php } ?>
                
              </tbody>
            </table>
          </div>
        </div>