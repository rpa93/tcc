<div class="col-md-9">
          <div class="content-box-header">
            <div class="panel-title">Consulta dos Projetos Aprovados</div>  
          </div>
          <div class="content-box-large box-with-header">
            <div class="row">
                  <!-- INPUT BUSCAR -->
                  <div class="col-md-6">
                    <div class="iconInput">
                      <form class="form-inline" role="form" method="post">
							<i class="fa fa-search" aria-hidden="true"></i>  
							<input type="search" id="search-input" class="form-control" placeholder="Buscar..." name="termo">
						</form>
                    </div>   
                  </div>
                  <!-- END INPUT BUSCAR -->
            </div>

            <div class="row">       
              <div class="col-md-12">
                    <table class="table table-striped table-bordered" id="example">
                      <thead>
                        <tr role="row"></tr>
                          <td class="sorting_asc" role="columheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Codigo: active to sort column descending">Código</td>
                          <td>Projeto</td>
                          <td>Área de Pesquisa</td>
                          <td>Ação</td>
                        </tr>  
                      </thead>
                      <tbody>
                      
                      <?php if($info->num_rows() >0){ ?>
						<?php foreach($info->result() as $linha){ ?>
                        <tr>
                          <th scope="row"><?php echo $linha->idProjeto; ?></th>
                          <td><?php echo $linha->nome; ?></td>
                          <td><?php if(isset($area_pesquisa[$arquivos[$linha->idProjeto]->idAreaPesquisa])) echo $area_pesquisa[$arquivos[$linha->idProjeto]->idAreaPesquisa]->nomeArea; ?></td>
                          <td>
                            <?php if(isset($arquivos[$linha->idProjeto])){ ?><a type="button" class="btn btn-success btn-modal"  href="<?php echo base_url().'public/uploads/'.$arquivos[$linha->idProjeto]->nome; ?>" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a><?php } ?>
                          </td>
                        </tr>
    					<?php } ?>
                    <?php } ?>
                        
                      </tbody>
                    </table>
              </div><!-- end col-md-12 --> 
            </div><!-- end row -->   
            <div class="row">  
              <div class="col-md-offset-6">   
                      <?php if(isset($paginacao)) echo $paginacao;?>
              </div>    
            </div><!-- end row -->       
          </div><!-- end content-box-large box-with-header -->
        </div>
        