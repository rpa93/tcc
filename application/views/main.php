<!DOCTYPE html>
<html>
  <head>
    <title>SGP<?php echo $this->session->userdata('tipo'); ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="<?php echo base_url().'public/';?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url().'public/';?>css/styles.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url().'public/';?>font-awesome-4.7.0/css/font-awesome.min.css">

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="<?php echo base_url().'public/';?>vendors/form-helpers/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/tags/css/bootstrap-tags.css" rel="stylesheet">

    <link href="<?php echo base_url().'public/';?>css/form.css" rel="stylesheet">
    
    <?php if(isset($pagina_atual) && $pagina_atual == 'projetos-liberados'){?>
	<link href="<?php echo base_url().'public/';?>css/wizard.css" rel="stylesheet">
 	<?php } ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    <!-- selecionar orientador ou aluno com a tag select -->
    <script type="text/javascript">         
      function showDiv(div)
      {
        document.getElementById("orientador").className = "invisivel";
        document.getElementById("aluno").className = "invisivel";

        document.getElementById(div).className = "visivel";
      }
    </script>

    <style>
      .invisivel { display: none; }
      .visivel { visibility: visible; }
    </style>



  </head>
  <body>
    
    <?php echo $page_header; ?>

    <div class="page-content">
      <div class="row">
      
		<?php echo $page_nav; ?>
		<?php echo $page_main; ?>
               
      </div><!-- end row -->   
    </div><!-- end page content -->


    <?php echo $page_modals; ?>



	<?php echo $page_footer; ?>
	
    
	<script>
		var BASE_URL = "<?php echo base_url(); ?>";
	</script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url().'public/';?>vendors/jquery-3.1.1.min.js"></script>
    <!-- jQuery UI -->
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url().'public/';?>bootstrap/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/form-helpers/js/bootstrap-formhelpers.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/select/bootstrap-select.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/tags/js/bootstrap-tags.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/mask/jquery.maskedinput.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/moment/moment.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

     <!-- bootstrap-datetimepicker -->
     <link href="<?php echo base_url().'public/';?>vendors/bootstrap-datetimepicker/datetimepicker.css" rel="stylesheet">
     <script src="<?php echo base_url().'public/';?>vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script> 


    <!--<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>--> 

    <script src="<?php echo base_url().'public/';?>js/custom.js"></script>
    <script src="<?php echo base_url().'public/';?>js/forms.js"></script>
    <script src="<?php echo base_url().'public/';?>js/wizard.js"></script>
  </body>
</html>