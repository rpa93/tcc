<!DOCTYPE html>
<html>
  <head>
    <title>SGPAdmin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="<?php echo base_url().'public/';?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="<?php echo base_url().'public/';?>css/styles.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url().'public/';?>font-awesome-4.7.0/css/font-awesome.min.css">

    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="<?php echo base_url().'public/';?>vendors/form-helpers/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/select/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>vendors/tags/css/bootstrap-tags.css" rel="stylesheet">

    <link href="<?php echo base_url().'public/';?>css/form.css" rel="stylesheet">
    <link href="<?php echo base_url().'public/';?>css/wizard.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    <!-- selecionar orientador ou aluno com a tag select -->
    <script type="text/javascript">         
      function showDiv(div)
      {
        document.getElementById("orientador").className = "invisivel";
        document.getElementById("aluno").className = "invisivel";

        document.getElementById(div).className = "visivel";
      }
    </script>

    <style>
      .invisivel { display: none; }
      .visivel { visibility: visible; }
    </style>
  </head>
  <body>
    <!-- MENU TOP -->
  	<div class="header">
	    <div class="container">
	      <div class="row">
	        <div class="col-md-5">
	          <!-- Logo -->
  	        <div class="logo">
  	          <h1><a href="home.html">SGP</a></h1>
  	        </div>
	        </div>
	        <div class="col-md-2 col-md-offset-5">
	          <div class="navbar navbar-inverse" role="banner">
	            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	             
	            </nav>
	          </div>
	        </div>
	      </div><!-- end row --> 
	    </div><!-- end container --> 
	  </div><!-- end header -->

    <div class="page-content">
      <div class="row">
        <div class="container">
          <div class="login">
            <div class="content-box-header">
              <div class="panel-title">Login</div>
            </div>
            <div class="content-box-large box-with-header">
              <div class="row">
                  <div class="col-sm-12 col-md-10  col-md-offset-1 ">
                  	<form method="post" action="">
                    <div class="form-group">
                    	<?php if(isset($msg) && $msg != ''){ echo $msg; } ?>
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="glyphicon glyphicon-user"></i>
                        </span> 
                        <input class="form-control" placeholder="Matricula" name="matricula" type="text" autofocus>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="glyphicon glyphicon-lock"></i>
                        </span>
                        <input class="form-control" placeholder="password" name="password" type="password" value="">
                      </div>
                    </div>
                    <div class="form-group">
                      <input type="submit" class="btn btn-lg btn-primary btn-block" value="Acessar">
                    </div>
                    </form>
                  </div>
              </div>
            </div>
          </div>
        </div>  
      </div>
    </div>

    <footer>
      <div class="container">   
        <div class="copy text-center">
          Copyright 2017 <a href='#'>SGP</a>
        </div>      
      </div>
    </footer>

   
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- jQuery UI -->
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url().'public/';?>bootstrap/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/form-helpers/js/bootstrap-formhelpers.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/select/bootstrap-select.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/tags/js/bootstrap-tags.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/mask/jquery.maskedinput.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/moment/moment.min.js"></script>

    <script src="<?php echo base_url().'public/';?>vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

     <!-- bootstrap-datetimepicker -->
     <link href="<?php echo base_url().'public/';?>vendors/bootstrap-datetimepicker/datetimepicker.css" rel="stylesheet">
     <script src="<?php echo base_url().'public/';?>vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script> 


    <!--<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>--> 

    <script src="<?php echo base_url().'public/';?>js/custom.js"></script>
    <script src="<?php echo base_url().'public/';?>js/forms.js"></script>
    <script src="<?php echo base_url().'public/';?>js/wizard.js"></script>
  </body>
</html>