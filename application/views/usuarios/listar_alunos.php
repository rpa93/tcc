		
      

            <div class="col-md-9">
              <div class="content-box-header">
               <div class="panel-title">Alunos</div>
              </div>
              <div class="content-box-large box-with-header">

                    <div id="aluno" class="">
                      <div class="row">
                        <div class="col-md-4"> 
                          <!-- BUTTON MODAL -->
                          <button type="button" id="cadastrar" class="btn btn-primary btn-modal" data-toggle="modal" data-target="#Modal" data-src="<?php echo base_url().'Usuario/formularioAluno'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar Novo Aluno</button>
                        </div>
                        <!-- INPUT BUSCAR -->
                        <div class="col-md-3 col-md-offset-4">
                          <div class="iconInput">
                            <form class="form-inline" role="form" method="post">
	                            <i class="fa fa-search" aria-hidden="true"></i>  
	                            <input type="search" id="search-input" class="form-control" placeholder="Buscar..." name="termo">
	                          </form>
                          </div>   
                        </div>
                        <!-- END INPUT BUSCAR -->
                      </div><!-- end row -->
                      <div class="row">   
                        <div class="col-md-12">  
                          <table class="table table-striped table-bordered">
                              <thead>
                                <tr role="row">
                                  <td>Código</td>
                                  <td>Matrícula</td>
                                  <td>CPF</td>
                                  <td>Nome</td>
                                  <td>Ação</td>
                                </tr>  
                              </thead>
                              <tbody>
                              
                              <?php if($lista_alunos->num_rows() >0){ ?>
                              		<?php foreach($lista_alunos->result() as $row){ ?>  
                                <tr>
                                  <th scope="row"><?php echo $row->idUsuario; ?></th>
	                                  <td><?php echo $row->matricula; ?></td>
	                                  <td><?php echo $row->cpf; ?></td>
	                                  <td><?php echo $row->nome; ?></td>
	                                  <td>
	                                    <button type="button" class="btn btn-success btn-modal" data-toggle="modal"  data-target="#Modal" data-src="<?php echo base_url().'Usuario/formularioAluno/'.$row->idUsuario; ?>"><i class="fa fa-eye" aria-hidden="true"></i></button>
	                                    <button type="button" class="btn btn-warning btn-modal" data-toggle="modal"  data-target="#Modal" data-src="<?php echo base_url().'Usuario/formularioAluno/'.$row->idUsuario; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></button>
	                                    <a href="<?php echo base_url().'Usuario/deletaAluno/'.$row->idUsuario; ?>"" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
	                                  </td>
                                </tr>
                                <?php } ?>
							<?php }else{ ?>
                               	<tr>
	                        		<th scope="row" colspan="5">Nenhum resultado encontrado</th>
								</tr>
							<?php } ?>
                                
                              </tbody>
                            </table>
                        </div><!-- end col-md-12 -->      
                      </div><!-- end row -->
                      
                        
                      <div class="row">
                        <div class="col-md-12">
                          <div class="col-md-offset-6">   
                            <?php if(isset($paginacao)) echo $paginacao;?>
                          </div> 
                        </div> 
                      </div>
                       
                      
                      
                    </div><!-- end aluno -->
              </div>
            </div><!-- end col -->