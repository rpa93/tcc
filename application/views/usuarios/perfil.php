<div class="col-md-9">
<form class="form-horizontal" method="post" action="">
		 <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Cadastro</h4>
          </div>
          <div class="modal-body">
                  <fieldset>
                  	<input type="hidden" name="idUsuario" value="<?php if(isset($info)) echo $info->idUsuario; ?>">
                  
                  
                    <legend>Dados pessoais</legend>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputMatricula">Matrícula:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="Matricula" type="text" name="matricula" value="<?php if(isset($info)) echo $info->matricula; ?>">
                      </div>
                    </div>

                   <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputCPF">CPF:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="CPF" type="text" name="cpf" value="<?php if(isset($info)) echo $info->cpf; ?>" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputName1">Nome:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="Nome" type="text" name="nome" value="<?php if(isset($info)) echo $info->nome; ?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-md-2 control-label">Sexo:</label>
                      <div class="col-md-10">
                        <div class="radio">
                          <label class="radio-inline">
                            <input type="radio" value='f' name="sexo" <?php if(isset($info) && $info->sexo == 'f') echo ' checked="checked"'; ?>>Feminino
                          </label>
                          <label class="radio-inline">
                            <input type="radio" value='m' name="sexo" <?php if(isset($info) && $info->sexo == 'm') echo ' checked="checked"'; ?>>Masculino
                          </label>  
                        </div>  
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputName2">Data de Nascimento:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="data" type="date" name="dataNasc" value="<?php if(isset($info)) echo $info->dataNasc; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputEmail1">E-mail:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="E-mail" type="email" name="email" value="<?php if(isset($info)) echo $info->email; ?>">
                      </div>
                    </div>
                  </fieldset>
                  
                  <fieldset>
                    <legend>Endereço</legend>
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputCEP">CEP:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="CEP" type="text" name="cep" value="<?php if(isset($info)) echo $info->cep; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputName2">Endereço:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="Endereço" type="text" name="endereco" value="<?php if(isset($info)) echo $info->endereco; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-2 control-label" for="exampleInputTel">Telefone:</label>
                      <div class="col-md-10">
                        <input class="form-control" placeholder="Telefone" type="text" name="telefone" value="<?php if(isset($info)) echo $info->telefone; ?>" />
                      </div>
                    </div>
                    
                  </fieldset>
                
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Salvar</button>
          </div>
          </form>
          </div>