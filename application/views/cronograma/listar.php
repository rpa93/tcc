<div class="col-md-9">
          <div class="content-box-header">
            <div class="panel-title">Reunião</div>
          </div>
          <div class="content-box-large box-with-header">
            <div class="row">  
                <div class="col-md-4"> 
                  <!-- BUTTON MODAL -->
                  <?php if($this->session->userdata('tipo') == 'professor'){ ?>
                  	<button type="button" id="cadastrar" class="btn btn-primary btn-modal" data-toggle="modal" data-target="#Modal" data-src="<?php echo base_url().'Cronograma/formulario/'; ?>"><i class="fa fa-plus" aria-hidden="true"></i> Marcar Reunião</button>
                  <?php } ?>
                </div>   
                <!-- INPUT BUSCAR -->
                <div class="col-md-3 col-md-offset-4">
                  <div class="iconInput">
                    <form class="form-inline" role="form" method="post">
						<i class="fa fa-search" aria-hidden="true"></i>  
						<input type="search" id="search-input" class="form-control" placeholder="Buscar..." name="termo">
					</form>
                  </div>   
                </div>
                <!-- END INPUT BUSCAR -->
            </div>
            <div class="row">  
              <div class="col-md-12">
                <table class="table table-striped table-bordered" id="example">
                      <thead>
                        <tr role="row"></tr>
                          <td class="sorting_asc" role="columheader" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Codigo: active to sort column descending">Código</td>
                          <td>Data</td>
                          <td>horário</td>
                          <td>Assunto</td>
                          <td>Situacao</td>
                          <td>Ocorrência</td>
                          <td>Ação</td>
                        </tr>  
                      </thead>
                      <tbody>
                      
                      <?php if($info->num_rows() >0){ ?>
						<?php foreach($info->result() as $linha){ ?>
                        <tr>
                          <th scope="row"><?php echo $linha->idCronograma?></th>
                          <td><?php echo date('d/m/Y',  strtotime($linha->dataCronograma))?></td>
                          <td><?php echo $linha->hora?></td>
                          <td><?php echo $linha->assunto?></td>
                          <td><?php echo $linha->sitCronograma?></td>
                          <td><?php echo $linha->ocorrencia?></td>
                          <td>
                          <?php if (is_null($linha->sitCronograma) && $this->session->userdata('tipo') == 'aluno') {?>
                            <a type="button" class="btn btn-danger" href="<?php echo base_url().'Cronograma/alteraStatus/'.$linha->idCronograma.'/Recusado'; ?>">Recusar</a>
                            <a type="button" class="btn btn-primary" href="<?php echo base_url().'Cronograma/alteraStatus/'.$linha->idCronograma.'/Confirmado'; ?>">Confirmar</a>
                            <?php } ?>
                            
                            <?php if($this->session->userdata('tipo') == 'professor'){ ?>
                            <button type="button" class="btn btn-warning btn-modal" data-toggle="modal" data-target="#Modal" data-src="<?php echo base_url().'Cronograma/formulario/'.$linha->idCronograma; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            <a href="<?php echo base_url().'Cronograma/deletar/'.$linha->idCronograma; ?>"" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            <?php } ?>
                          </td>
                        </tr>
                        
                            <?php } ?>
                    <?php } ?>
                        
                      </tbody>
                    </table>
              </div>      
            </div><!-- row -->  
            <div class="row">
                <div class="col-md-12">
                  <div class="col-md-offset-6">   
                    <?php if(isset($paginacao)) echo $paginacao;?>
                  </div> 
                </div> 
              </div>                
          </div><!-- content -->
        </div><!-- col-md-9 -->