<div class="col-md-3">

          <div class="sidebar content-box" style="display: block;">
            <ul class="nav">
              <!-- Main menu -->
              <li class="current">
              <a href="<?php echo base_url().'home/' ?>">
              	<i class="glyphicon glyphicon-home"></i> Home
              </a>
              </li>
              
              
              <?php if($this->session->userdata('tipo') == 'admin'){ ?>
              <li class="submenu">
                <a href="">
                  <i class="fa fa-key" aria-hidden="true"></i> Cadastro de Usuário
                  <span class="caret pull-right"></span>                
                </a>
                  <!-- Sub menu -->
                  <ul>
                    <li>
                      <a href="<?php echo base_url().'usuario/alunos' ?>">
                        <i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp; Alunos
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo base_url().'usuario/orientadores' ?>">
                        <i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp; Professores
                      </a>
                    </li>
                  </ul>
              </li>
              <?php }?>
              
              <?php if($this->session->userdata('tipo') != 'admin'){ ?>
              <li class="submenu">
                <a href="">
                  <i class="fa fa-book"></i> Acadêmico
                  <span class="caret pull-right"></span>
                </a>
                <!-- Sub menu -->
                <ul>
                  <li>
                    <a href="<?php echo base_url().'Usuario/perfil/' ?>" class="perfil">
                      <i class="fa fa-id-card-o"></i>&nbsp;&nbsp; Perfil
                    </a>
                  </li>
                </ul>
              </li>
              <?php } ?>
              
              
              
              <li class="submenu">
                  <a href="#">
                    <i class="fa fa-mortar-board"></i> Projetos
                    <span class="caret pull-right"></span>
                  </a>
                  <!-- Sub menu -->
                  <?php if($this->session->userdata('tipo') == 'professor'){ ?>
	                  <ul>
	                    <li>
	                      <a href="<?php echo base_url().'AreaDePesquisa/listar/' ?>"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp; Cadastrar área de pesquisa</a>
	                    </li>
	                  </ul>
	                  <ul>
	                    <li>
	                      <a href="<?php echo base_url().'Tema/listar/' ?>"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp; Cadastrar Tema</a>
	                    </li>
	                  </ul>
	                  <ul>
	                    <li>
	                      <a href="<?php echo base_url()."Projeto/analisar"?>"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp; Analisar Proposta</a>
	                    </li>
	                  </ul>
                  <?php } ?>
                  
                  
                  
                  <?php if($this->session->userdata('tipo') == 'aluno'){ ?>
	                  <ul>
	                    <li>
	                      <a href="<?php echo base_url().'Projeto/inscricao/'?>"><i class="fa fa-clipboard"></i>&nbsp;&nbsp; Formulário de Inscrição</a>
	                    </li>
	                  </ul>
                  <?php }?>
                  
                  <?php if(($this->session->userdata('tipo') == 'aluno') || ($this->session->userdata('tipo') == 'professor')){ ?>
                  	<ul>
	                    <li>
	                      <a href="<?php echo base_url().'Projeto/aprovados/'?>"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp; Projetos aprovados</a>
	                    </li>
	                  </ul>
	                  <ul>
	                    <li>
	                      <a href="<?php echo base_url()."Cronograma/listar/"?>"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp; Reunião</a>
	                    </li>
	                  </ul>
                  <?php }?>
                  
                  <?php if(($this->session->userdata('tipo') == 'admin')){ ?>
	                  <ul>
	                    <li>
	                      <a href="<?php echo base_url().'Projeto/liberados'?>"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp; Lista de Projetos Liberados</a>
	                    </li>
	                  </ul>
                  <?php }?>

					<ul>
	                    <li>
	                      <a href="<?php echo base_url().'Projeto/finalizados'?>"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp; Lista de Projetos Finalizados</a>
	                    </li>
	                  </ul>
              </li>
              
              <?php if(($this->session->userdata('tipo') == 'aluno') || ($this->session->userdata('tipo') == 'professor')){ ?>
              <li class="submenu">
                <a href="">
                  <i class="fa fa-clipboard"></i> Repositório
                  <span class="caret pull-right"></span>
                </a>
                <!-- Sub menu -->
                <?php if(($this->session->userdata('tipo') == 'aluno')){ ?>
                <ul>
                  <li>
                    <a href="<?php echo base_url().'Upload/listar'; ?>" class="perfil">
                      <i class="fa fa-files-o"></i>&nbsp;&nbsp; Upload do Projeto
                    </a>
                  </li>
                </ul>
                <?php } ?>
                
                <?php if(($this->session->userdata('tipo') == 'professor')){ ?>
	                <ul>
	                  <li>
	                    <a href="<?php echo base_url().'Upload/listar'; ?>" class="perfil">
	                      <i class="fa fa-files-o"></i>&nbsp;&nbsp; Ver Uploads de Projetos
	                    </a>
	                  </li>
	                </ul>
                <?php } ?>
                
              </li>
              <?php } ?>
              
              <?php if(($this->session->userdata('tipo') == 'professor')){ ?>
              <li class="submenu">
                  <a href="#">
                    <i class="fa fa-mortar-board"></i> Banca
                    <span class="caret pull-right"></span>
                  </a>
                  <!-- Sub menu -->
                  <ul>
                    <li>
                      <a href="<?php echo base_url().'Projeto/liberar/'; ?>"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;&nbsp; Liberar Projeto</a>
                    </li>
                  </ul>
                  <ul>
                    <li>
                      <a href="<?php echo base_url().'Projeto/info/'; ?>"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;&nbsp; Informações</a>
                    </li>
                  </ul>
              </li>
              <?php } ?>
            </ul>
          </div><!-- end sidebar -->
        </div><!-- end col-md-3 -->