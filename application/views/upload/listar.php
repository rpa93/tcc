		<div class="col-md-9">
		<?php if($this->session->userdata('tipo') == 'aluno'){?>
          <div class="content-box-header">
            <div class="panel-title">Upload</div>
          </div>
          
          <div class="content-box-large box-with-header">
            <div class="row">  
              <div class="col-md-12">
              
              	<form class="form-horizontal" method="post" enctype="multipart/form-data" action="">
    				<div class="col-md-12">
    					<label class="control-label">Projeto:</label><br>
    					<select name="idProjeto">
                        	<?php foreach($projetos as $linha){?>
                        		<option value="<?php echo $linha->idProjeto; ?>"><?php echo $linha->nome; ?></option>
                        	<?php } ?>
                        </select><br>
    				
    				  
	                    <label class="control-label">Upload do Projeto</label>
	                    <input id="input-folder-2" name="userfile[]" class="file-loading" type="file" multiple> <!-- webkitdirectory accept="image/*"  -->
                    	<div id="errorBlock" class="help-block"></div>
                  </div>
                  <button type="submit" class="btn btn-primary">Enviar</button>
				</form>
              </div>
            </div>  
          </div>
     	<?php } ?>
          <div class="content-box-header">
            <div class="panel-title">Material</div>
          </div>
          <div class="content-box-large box-with-header">
            <div class="row">  
              <div class="col-md-12">
                <table class="table table-condensed">
                  <thead>
                    <tr onclick="location.href = 'http://www.site.com';" style="cursor: hand;">
                      <th>Nome</th>
                      <th>Projeto</th>
                      <th>Data de Inclusão</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php if($info->num_rows() >0){ ?>
						<?php foreach($info->result() as $linha){ ?>
                    <tr>
                      <td><a href="<?php echo base_url().'public/uploads/'.$linha->nome;?>" target="_blank"><i id="icon" class="glyphicon glyphicon-folder-close"></i>&nbsp;&nbsp; Arquivo</a></td>
                      <td><?php echo $projetos_enviados[$linha->idProjeto]->nome; ?></td>
                      <td><?php echo Utilidades::date_formato($linha->criado, true); ?></td>
                      <td><a href="<?php echo base_url().'Upload/deletar/'.$linha->idArquivo; ?>" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                    </tr>
                    <?php } ?>
				<?php } ?>
                  </tbody>
                </table>
              </div>
            </div>  
          </div>
        </div>